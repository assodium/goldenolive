import pandas as pd
import os
import sqlite3
queries = {
    'tbl_list': "SELECT name FROM sqlite_master WHERE type IN ('table', 'view') AND name NOT LIKE 'sqlite_%' UNION ALL SELECT name FROM sqlite_temp_master WHERE type IN ('table', 'view') ORDER BY 1"
    # 'get_tbl' : "SELECT * FROM {}" #.format('NextChip'),
    # 'tbl_rename' : "alter table {} rename to {}"
    # 'get_cols : SELECT sql FROM sqlite_master WHERE tbl_name='myTable'
}
import matplotlib.pyplot as plt
from matplotlib import font_manager, rc
font_name = font_manager.FontProperties(fname="c:/Windows/Fonts/gulim.ttc").get_name()
rc('font', family=font_name)
rc('xtick', labelsize = 6)
rc('ytick', labelsize = 6)
plt.rcParams["figure.figsize"] = (18,9)

DB_NAME = 'db/hoga_0921_080204.db'
IMG_PATH  = './pics/'+DB_NAME[8:-10]+'/'
os.makedirs(IMG_PATH, exist_ok=True)

pdb = sqlite3.connect(DB_NAME)
c = pdb.cursor()
#Get Table list
c.execute(queries['tbl_list'])
tbl_list = c.fetchall()
print('Stock List : {}'.format(tbl_list))

def get_volatility(stock):
    df = pd.read_sql('SELECT * from [{}]'.format(stock), pdb)

    floor_price = df['high'].iloc[-1]
    bottom_price = df['low'].iloc[-1]
    open_price = df['open'].iloc[-1]

    res = int(( (floor_price - bottom_price) / open_price) * 100 ) * 0.01  
    print('{} Volatility : {}'.format(stock, res))
    return res

def draw_plot(stock):
    print('DB Plot @ {}'.format(stock))
    df = pd.read_sql('SELECT * from [{}]'.format(stock), pdb)

    fig, axs = plt.subplots(4, 1)

    fig.autofmt_xdate(rotation=90)
    #1. Title
    fig.suptitle('{} {} ({}~)'.format(stock, df['date'].iloc[0], df['cur_time'].iloc[0][:-2]))

    #2. X Tick
    df['xtick'] = df['cur_time']
    for i in range(len(df)):
        df['xtick'].iloc[i] = df['xtick'].iloc[i][:-2]

    #3. Limit of Time
    df = df.loc[df['xtick']<='1530']

    #4. Draw Plots
    # axs[0].set_ylim(-15000, 15000)
    axs[0].plot(df['offervol']-df['bidvol'], color='blue', label ='vol delta')
    axs[0].set
    axs[0].legend()
    axs[0].grid(True)

    axs[1].plot(df['totOffer']-df['totBid'], color='red', label ='tot_Vol delta')
    axs[1].legend()
    axs[1].grid(True)

    axs[2].plot(df['vol_str'], color='black', label='strength')
    axs[2].legend()
    axs[2].grid(True)

    axs[3].plot(df['xtick'], df['cur'], color='black', label='price')
    # axs[3].set_xticklabels(rotation = 90)
    axs[3].legend()
    axs[3].grid(True)

    plt.tight_layout()
    plt.show()

    # filename = '{}_{}'.format(stock, df['date'].iloc[0])
    # plt.savefig(IMG_PATH + filename +'.png', dpi=72)
    # plt.clf()
    
if __name__ == '__main__':


#1. Get Volatility
    # df_volatility = pd.DataFrame([], columns = ['name', 'volatility'])
    # for stockname in tbl_list:
    #     # print(stockname[0])
    #     # draw_plot(stockname[0])
    #     new_df = {'name': stockname[0], 'volatility': get_volatility(stockname[0])}
    #     df_volatility = df_volatility.append(new_df, ignore_index=True)
    
    # df_volatility = df_volatility.sort_values(by = 'volatility', ascending = False)
    # df_volatility.to_csv('df_volatility.csv') 

#2. Single Plot
    stock_name = '랩지노믹스'
    draw_plot(stock_name)


