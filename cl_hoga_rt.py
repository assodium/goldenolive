DEBUG_MODE = False
from pickletools import stringnl
from sys import _current_frames
from numpy import append
# from signal import strsignal
import pandas as pd
import win32com.client
from pandas import Series, DataFrame, read_sql
import locale
import os
import sqlite3
import datetime
import time

# cp object
g_objCodeMgr = win32com.client.Dispatch("CpUtil.CpCodeMgr")
g_objCpStatus = win32com.client.Dispatch("CpUtil.CpCybos")
g_objCpTrade = win32com.client.Dispatch("CpTrade.CpTdUtil")
locale.setlocale(locale.LC_ALL, '')

# 현재가 정보 저장 구조체
class stockPricedData:
    def __init__(self):
        self.dicEx = {ord('0'): "동시호가/장중 아님", ord('1'): "동시호가", ord('2'): "장중"}
        self.code = ""
        self.name = ""
        self.cur = 0        # 현재가
        self.diff = 0       # 대비
        self.diffp = 0      # 대비율
        self.offer = [0 for _ in range(10)]     # 매도호가
        self.bid = [0 for _ in range(10)]       # 매수호가
        self.offervol = [0 for _ in range(10)]     # 매도호가 잔량
        self.bidvol = [0 for _ in range(10)]       # 매수호가 잔량
        self.totOffer = 0       # 총매도잔량
        self.totBid = 0         # 총매수 잔량
        self.vol = 0            # 거래량
        self.tvol = 0           # 순간 체결량
        self.baseprice = 0      # 기준가
        self.high = 0
        self.low = 0
        self.open = 0
        self.volFlag = ord('0')  # 체결매도/체결 매수 여부
        self.time = 0
        self.sum_buyvol = 0
        self.sum_sellvol = 0
        self.vol_str = 0

        # 예상체결가 정보
        self.exFlag= ord('2')
        self.expcur = 0         # 예상체결가
        self.expdiff = 0        # 예상 대비
        self.expdiffp = 0       # 예상 대비율
        self.expvol = 0         # 예상 거래량
        self.objCur = CpPBStockCur()
        self.objOfferbid = CpPBStockBid()
    def __del__(self):
        print('stockPricedData del() : Unsubscribe CpPBStockCur, CpPBStockBid')
        self.objCur.Unsubscribe()
        self.objOfferbid.Unsubscribe()

# SB/PB 요청 ROOT 클래스
class CpPublish:
    def __init__(self, name, serviceID):
        self.name = name
        self.obj = win32com.client.Dispatch(serviceID)
        self.bIsSB = False
    def Subscribe(self, var, rpMst, parent):
        print('C@ : CpPublish - Subscribe')
        if self.bIsSB:
            self.Unsubscribe()

        if (len(var) > 0):
            self.obj.SetInputValue(0, var)

        handler = win32com.client.WithEvents(self.obj, CpEvent)
        handler.set_params(self.obj, self.name, rpMst, parent)
        self.obj.Subscribe() #gwan 22.8.5
        self.bIsSB = True #gwan 22.8.5
    def Unsubscribe(self):
        print('C@ : CpPublish - unSubscribe')
        if self.bIsSB:
            self.obj.Unsubscribe()
        self.bIsSB = False

# CpPBStockCur: 실시간 현재가 요청 클래스
class CpPBStockCur(CpPublish):
    def __init__(self):
        super().__init__("stockcur", "DsCbo1.StockCur")

# CpPBStockBid: 실시간 10차 호가 요청 클래스
class CpPBStockBid(CpPublish):
    def __init__(self):
        super().__init__("stockbid", "Dscbo1.StockJpBid")

# CpRPCurrentPrice:  현재가 기본 정보 조회 클래스
class CpRPCurrentPrice:
    def __init__(self):
        if (g_objCpStatus.IsConnect == 0):
            print("PLUS가 정상적으로 연결되지 않음. ")
            return
        self.objStockMst = win32com.client.Dispatch("DsCbo1.StockMst")
        return
    def Request(self, code, rtMst, callbackobj):
        # 현재가 통신
        rtMst.objCur.Unsubscribe()
        rtMst.objOfferbid.Unsubscribe()

        self.objStockMst.SetInputValue(0, code)
        ret = self.objStockMst.BlockRequest()
        if self.objStockMst.GetDibStatus() != 0:
            print("통신상태", self.objStockMst.GetDibStatus(), self.objStockMst.GetDibMsg1())
            return False

        # 수신 받은 현재가 정보를 rtMst 에 저장
        rtMst.code = code
        rtMst.name = g_objCodeMgr.CodeToName(code)
        rtMst.cur =  self.objStockMst.GetHeaderValue(11)  # 종가
        rtMst.diff =  self.objStockMst.GetHeaderValue(12)  # 전일대비
        if rtMst.cur-rtMst.diff != 0:
            rtMst.diffp = int(rtMst.diff/(rtMst.cur-rtMst.diff) * 10000) / 100
        else :
            rtMst.diffp = 9999 
        rtMst.baseprice  =  self.objStockMst.GetHeaderValue(27)  # 기준가
        rtMst.vol = self.objStockMst.GetHeaderValue(18)  # 거래량
        rtMst.exFlag = self.objStockMst.GetHeaderValue(58)  # 예상플래그
        rtMst.expcur = self.objStockMst.GetHeaderValue(55)  # 예상체결가
        rtMst.expdiff = self.objStockMst.GetHeaderValue(56)  # 예상체결대비

        rtMst.totOffer = self.objStockMst.GetHeaderValue(71)  # 총매도잔량
        rtMst.totBid = self.objStockMst.GetHeaderValue(73)  # 총매수잔량

        # 10차호가
        for i in range(10):
            rtMst.offer[i] = (self.objStockMst.GetDataValue(0, i))  # 매도호가
            rtMst.bid[i] = (self.objStockMst.GetDataValue(1, i) ) # 매수호가
            rtMst.offervol[i] = (self.objStockMst.GetDataValue(2, i))  # 매도호가 잔량
            rtMst.bidvol[i] = (self.objStockMst.GetDataValue(3, i) ) # 매수호가 잔량

        # if DEBUG_MODE:
        print('[F:Request] {}, {}, 종가:{}, 매도호가:{}, 매도잔량:{}, 총매도잔량:{}, 매수호가:{}, 매수잔량:{}, 총매수잔량:{}'.format( \
            rtMst.code, rtMst.name, rtMst.cur, rtMst.offer, rtMst.offervol, rtMst.totOffer, rtMst.bid, rtMst.bidvol, rtMst.totBid))

        #실시간 이벤트 콜백 호출
        rtMst.objCur.Subscribe(code, rtMst, callbackobj)
        rtMst.objOfferbid.Subscribe(code, rtMst, callbackobj)

# CpEvent: 실시간 이벤트 수신 클래스
class CpEvent:
    def set_params(self, client, name, rpMst, parent):
        self.client = client  # CP 실시간 통신 object
        self.name = name  # 서비스가 다른 이벤트를 구분하기 위한 이름
        self.parent = parent  # callback 을 위해 보관
        self.rpMst = rpMst
    # PLUS 로 부터 실제로 시세를 수신 받는 이벤트 핸들러
    def OnReceived(self):
        print('RT Event RCV: ', self.name)
        if self.name == "stockcur":
            # 현재가 체결 데이터 실시간 업데이트
            self.rpMst.exFlag = self.client.GetHeaderValue(19)  # 예상체결 플래그
            code = self.client.GetHeaderValue(0)
            diff = self.client.GetHeaderValue(2)
            vol = self.client.GetHeaderValue(9)  # 거래량
            cur= self.client.GetHeaderValue(13)  # 현재가
            
            diffp = int(diff/(cur-diff) * 10000) / 100

            # 예제는 장중만 처리 함.
            if (self.rpMst.exFlag == ord('1')):  # 동시호가 시간 (예상체결)     
                # 예상체결가 정보
                self.rpMst.expcur = cur
                self.rpMst.expdiff = diff   
                self.rpMst.expvol = vol
            else:
                self.rpMst.cur = cur
                self.rpMst.diff = diff
                self.rpMst.diffp = diffp
                self.rpMst.vol = vol
                self.rpMst.open = self.client.GetHeaderValue(4)
                self.rpMst.high = self.client.GetHeaderValue(5)
                self.rpMst.low = self.client.GetHeaderValue(6)
                self.rpMst.tvol = self.client.GetHeaderValue(17)
                self.rpMst.volFlag = self.client.GetHeaderValue(14)  # '1'  매수 '2' 매도
                self.rpMst.time = self.client.GetHeaderValue(18)
                self.rpMst.sum_buyvol = self.client.GetHeaderValue(16)  #누적매수체결수량 (체결가방식)
                self.rpMst.sum_sellvol = self.client.GetHeaderValue(15) #누적매도체결수량 (체결가방식)
                if (self.rpMst.sum_sellvol) :
                    self.rpMst.volstr = self.rpMst.sum_buyvol / self.rpMst.sum_sellvol * 100
                else :
                    self.rpMst.volstr = 0
            return

        elif self.name == "stockbid":
            # 현재가 10차 호가 데이터 실시간 업데이c
            code = self.client.GetHeaderValue(0)
            dataindex = [3, 7, 11, 15, 19, 27, 31, 35, 39, 43]
            obi = 0
            for i in range(10):
                self.rpMst.offer[i] = self.client.GetHeaderValue(dataindex[i])
                self.rpMst.bid[i] = self.client.GetHeaderValue(dataindex[i] + 1)
                self.rpMst.offervol[i] = self.client.GetHeaderValue(dataindex[i] + 2)
                self.rpMst.bidvol[i] = self.client.GetHeaderValue(dataindex[i] + 3)

            self.rpMst.totOffer = self.client.GetHeaderValue(23)
            self.rpMst.totBid = self.client.GetHeaderValue(24)
            return

# CpPBCssAlert: 종목검색 실시간 PB 클래스 
class CpPBCssAlert(CpPublish):
    def __init__(self):
        super().__init__('cssalert', 'CpSysDib.CssAlert')

# Cp8537 : 종목검색 전략 조회
class Cp8537:
    def __init__(self):
        self.objpb = CpPBCssAlert()
        self.bisSB = False
        self.monList = {}

    def __del__(self):
        self.Clear()

    def Clear(self):
        self.stopAllStgControl()
        if self.bisSB:
            self.objpb.Unsubscribe()
            self.bisSB = False

    def requestList(self, sel):
        retStgList = {}
        objRq = win32com.client.Dispatch("CpSysDib.CssStgList")

        # 예제 전략에서 전략 리스트를 가져옵니다.
        if (sel == '예제') :
            objRq.SetInputValue(0, ord('0'))   # '0' : 예제전략, '1': 나의전략
        else :
            objRq.SetInputValue(0, ord('1'))  # '0' : 예제전략, '1': 나의전략
        objRq.BlockRequest()

        # 통신 및 통신 에러 처리
        rqStatus = objRq.GetDibStatus()
        if rqStatus != 0:
            rqRet = objRq.GetDibMsg1()
            print("통신상태", rqStatus, rqRet)
            return (False, retStgList)

        cnt = objRq.GetHeaderValue(0) # 0 - (long) 전략 목록 수
        flag = objRq.GetHeaderValue(1) # 1 - (char) 요청구분
        print('종목검색 전략수:', cnt)


        for i in range(cnt):
            item = {}
            item['전략명'] = objRq.GetDataValue(0, i)
            item['ID'] = objRq.GetDataValue(1, i)
            item['전략등록일시'] = objRq.GetDataValue(2, i)
            item['작성자필명'] = objRq.GetDataValue(3, i)
            item['평균종목수'] = objRq.GetDataValue(4, i)
            item['평균승률'] = objRq.GetDataValue(5, i)
            item['평균수익'] = objRq.GetDataValue(6, i)
            retStgList[item['전략명']] = item
            print(item)
            
        return (True, retStgList)

    def requestStgID(self, id):
        retStgList = []
        objRq = None
        objRq = win32com.client.Dispatch("CpSysDib.CssStgFind")
        objRq.SetInputValue(0, id) # 전략 id 요청
        objRq.BlockRequest()
        # 통신 및 통신 에러 처리
        rqStatus = objRq.GetDibStatus()
        if rqStatus != 0:
            rqRet = objRq.GetDibMsg1()
            print("통신상태", rqStatus, rqRet)
            return (False, retStgList)

        cnt = objRq.GetHeaderValue(0)  # 0 - (long) 검색된 결과 종목 수
        totcnt = objRq.GetHeaderValue(1)  # 1 - (long) 총 검색 종목 수
        stime = objRq.GetHeaderValue(2)  # 2 - (string) 검색시간
        print('검색된 종목수:', cnt, '전체종목수:', totcnt, '검색시간:', stime)

        for i in range(cnt):
            item = {}
            item['code'] = objRq.GetDataValue(0, i)
            item['종목명'] = g_objCodeMgr.CodeToName(item['code'])
            retStgList.append(item)

        return (True, retStgList)

    def requestMonitorID(self, id):
        objRq = win32com.client.Dispatch("CpSysDib.CssWatchStgSubscribe")
        objRq.SetInputValue(0, id) # 전략 id 요청
        objRq.BlockRequest()


        # 통신 및 통신 에러 처리
        rqStatus = objRq.GetDibStatus()
        if rqStatus != 0:
            rqRet = objRq.GetDibMsg1()
            print("통신상태", rqStatus, rqRet)
            return (False, 0)

        monID = objRq.GetHeaderValue(0)
        if  monID == 0:
            print('감시 일련번호 구하기 실패')
            return (False, 0)

        # monID - 전략 감시를 위한 일련번호를 구해온다.
        # 현재 감시되는 전략이 없다면 감시일련번호로 1을 리턴하고,
        # 현재 감시되는 전략이 있다면 각 통신 ID에 대응되는 새로운 일련번호를 리턴한다.
        return (True, monID)

    def requestStgControl(self, id, monID, bStart):
        objRq = win32com.client.Dispatch("CpSysDib.CssWatchStgControl")
        objRq.SetInputValue(0, id) # 전략 id 요청
        objRq.SetInputValue(1, monID)  # 감시일련번호

        if bStart == True:
            objRq.SetInputValue(2, ord('1'))  # 감시시작
            print('전략감시 시작 요청 ', '전략 ID:', id, '감시일련번호',monID)
        else:
            objRq.SetInputValue(2, ord('3'))  # 감시취소
            print('전략감시 취소 요청 ', '전략 ID:', id, '감시일련번호', monID)
        objRq.BlockRequest()


        # 통신 및 통신 에러 처리
        rqStatus = objRq.GetDibStatus()
        if rqStatus != 0:
            rqRet = objRq.GetDibMsg1()
            print("통신상태", rqStatus, rqRet)
            return (False, '')

        status = objRq.GetHeaderValue(0)

        if status == 0 :
            print('전략감시상태: 초기상태')
        elif status == 1 :
            print('전략감시상태: 감시중')
        elif status == 2 :
            print('전략감시상태: 감시중단')
        elif status == 3 :
            print('전략감시상태: 등록취소')

        # event 수신 요청 - 요청 중이 아닌 경우에만 요청
        # if self.bisSB == False:
        #     print('Now going to RT subscribe Mode.. But Today..')
        #     self.objpb.Subscribe('', self) #disable for Test : 8/4
        #     self.bisSB = True

        # 진행 중인 전략들 저장
        if bStart == True:
            self.monList[id] = monID
        else:
            if id in self.monList :
                del self.monList[id]

        return (True, status)

    def stopAllStgControl(self):
        delitem = []
        for id, monId in self.monList.items() :
            delitem.append((id,monId))

        for item in delitem:
            self.requestStgControl(item[0], item[1], False)

        print(len(self.monList))

    def checkRealtimeStg(self, pbData):
        # 감시중인 전략인 경우만 체크
        id = pbData['전략ID']
        monid = pbData['감시일련번호']
        if not (id in self.monList) :
            return

        if (monid != self.monList[id]) :
            return

        print(pbData)

def init_dataframe():
    df = pd.DataFrame(columns=['stgName','code', 'name', 'date', 'cur_time', 'cur', 'diff', 'diffp',
                                            'offer[0]', 'offer[1]', 'offer[2]', 'offer[3]', 'offer[4]', 'offer[5]', 'offer[6]', 'offer[7]', 'offer[8]', 'offer[9]',
                                            'bid[0]', 'bid[1]', 'bid[2]', 'bid[3]', 'bid[4]', 'bid[5]', 'bid[6]', 'bid[7]', 'bid[8]', 'bid[9]',
                                            'offervol[0]', 'offervol[1]', 'offervol[2]', 'offervol[3]', 'offervol[4]', 'offervol[5]', 'offervol[6]', 'offervol[7]', 'offervol[8]', 'offervol[9]',
                                            'bidvol[0]', 'bidvol[1]', 'bidvol[2]', 'bidvol[3]', 'bidvol[4]', 'bidvol[5]', 'bidvol[6]', 'bidvol[7]', 'bidvol[8]', 'bidvol[9]',
                                            'totOffer', 'totBid', 'vol', 'tvol', 'baseprice', 'high', 'low', 'open', 'volFlag', 'time', 'sum_buyvol', 'sum_sellvol', 'vol_str'
                                            ])
    return df

class hoga:
    def __init__(self, db_file):
        self.budget = 100000000
        self.max_budget = 100000000
        self.objMst = CpRPCurrentPrice()
        self.item = stockPricedData()

        self.stock_df = pd.DataFrame(columns=['stgName','code', 'name', 'date', 'cur_time', 'cur', 'diff', 'diffp',
                                            'offer[0]', 'offer[1]', 'offer[2]', 'offer[3]', 'offer[4]', 'offer[5]', 'offer[6]', 'offer[7]', 'offer[8]', 'offer[9]',
                                            'bid[0]', 'bid[1]', 'bid[2]', 'bid[3]', 'bid[4]', 'bid[5]', 'bid[6]', 'bid[7]', 'bid[8]', 'bid[9]',
                                            'offervol[0]', 'offervol[1]', 'offervol[2]', 'offervol[3]', 'offervol[4]', 'offervol[5]', 'offervol[6]', 'offervol[7]', 'offervol[8]', 'offervol[9]',
                                            'bidvol[0]', 'bidvol[1]', 'bidvol[2]', 'bidvol[3]', 'bidvol[4]', 'bidvol[5]', 'bidvol[6]', 'bidvol[7]', 'bidvol[8]', 'bidvol[9]',
                                            'totOffer', 'totBid', 'vol', 'tvol', 'baseprice', 'high', 'low', 'open', 'volFlag', 'time', 'sum_buyvol', 'sum_sellvol', 'vol_str'
                                            ])

        self.hoga_db = sqlite3.connect(db_file)
        self.db_driver = self.hoga_db.cursor()
        self.db_queries = {
            'tbl_list': "SELECT name FROM sqlite_master WHERE type IN ('table', 'view') AND name NOT LIKE 'sqlite_%' UNION ALL SELECT name FROM sqlite_temp_master WHERE type IN ('table', 'view') ORDER BY 1",
            'get_tbl' : "SELECT * FROM {}" #.format('NextChip')
        }
    def req_hoga(self, code):
        self.objMst.Request(code, self.item, self)
        time.sleep(1)
    def add_10hoga(self, stgName):
        now = datetime.datetime.now()
        cur_date = now.strftime('%y%m%d')
        cur_time = now.strftime('%H%M%S')
        if DEBUG_MODE: print("Data Time: ", cur_time)

        new_list = [(stgName, self.item.code, self.item.name, cur_date, cur_time, self.item.cur, self.item.diff, self.item.diffp,
                        self.item.offer[0], self.item.offer[1], self.item.offer[2], self.item.offer[3], self.item.offer[4], self.item.offer[5], self.item.offer[6], self.item.offer[7], self.item.offer[8], self.item.offer[9],
                        self.item.bid[0], self.item.bid[1], self.item.bid[2], self.item.bid[3], self.item.bid[4], self.item.bid[5], self.item.bid[6], self.item.bid[7], self.item.bid[8], self.item.bid[9],
                        self.item.offervol[0], self.item.offervol[1], self.item.offervol[2], self.item.offervol[3], self.item.offervol[4], self.item.offervol[5], self.item.offervol[6], self.item.offervol[7], self.item.offervol[8], self.item.offervol[9],
                        self.item.bidvol[0], self.item.bidvol[1], self.item.bidvol[2], self.item.bidvol[3], self.item.bidvol[4], self.item.bidvol[5], self.item.bidvol[6], self.item.bidvol[7], self.item.bidvol[8], self.item.bidvol[9],
                        self.item.totOffer, self.item.totBid, self.item.vol, self.item.tvol, self.item.baseprice, self.item.high, self.item.low, self.item.open, self.item.volFlag, self.item.time, self.item.sum_buyvol, self.item.sum_sellvol, self.item.vol_str,
                        )]
        new_df = pd.DataFrame(new_list, columns=['stgName','code', 'name', 'date', 'cur_time', 'cur', 'diff', 'diffp',
                                            'offer[0]', 'offer[1]', 'offer[2]', 'offer[3]', 'offer[4]', 'offer[5]', 'offer[6]', 'offer[7]', 'offer[8]', 'offer[9]',
                                            'bid[0]', 'bid[1]', 'bid[2]', 'bid[3]', 'bid[4]', 'bid[5]', 'bid[6]', 'bid[7]', 'bid[8]', 'bid[9]',
                                            'offervol[0]', 'offervol[1]', 'offervol[2]', 'offervol[3]', 'offervol[4]', 'offervol[5]', 'offervol[6]', 'offervol[7]', 'offervol[8]', 'offervol[9]',
                                            'bidvol[0]', 'bidvol[1]', 'bidvol[2]', 'bidvol[3]', 'bidvol[4]', 'bidvol[5]', 'bidvol[6]', 'bidvol[7]', 'bidvol[8]', 'bidvol[9]',
                                            'totOffer', 'totBid', 'vol', 'tvol', 'baseprice', 'high', 'low', 'open', 'volFlag', 'time', 'sum_buyvol', 'sum_sellvol', 'vol_str'
                                            ])
        self.stock_df = self.stock_df.append(new_df, ignore_index=True)
        print('NEWDF>> ', new_df)
        if DEBUG_MODE == True: print(self.stock_df) 
    def db_update(self, name):
        each_df = self.stock_df.loc[self.stock_df['name']==name]
        each_df.to_sql(name, self.hoga_db, if_exists='append', index=False)
        # each_df.to_sql(code, self.hoga_db, if_exists='append', index=False)
        # self.hoga_db.execute('ALTER TABLE 'K005930' ADD PRIMARY KEY ("cur_time");')
        print('DB updated')   
    def db_show(self, name):
        self.db_driver.execute("SELECT * FROM %s" % name)
        print('DB : ', self.db_driver.fetchall())
    def db_tbl_list(self):
        self.db_driver.execute(self.db_queries['tbl_list'])
        print('DB : ', self.db_driver.fetchall())
    def db_tbl_delete(self, tbl_name):
        self.db_driver.execute("DROP TABLE {}".format(tbl_name))
        print('DB Table Drop : {}'.format(tbl_name))
    def df_clean(self):
        self.stock_df = init_dataframe()
        print('Clean : DF')
    def db_close(self):
        self.hoga_db.commit()
        self.hoga_db.close()
        print('DB Close')
    def vol_ratio_sell_buy(self):
        ratio = 1 #def. ratio
        if sum(self.item.offervol[0:10]) > 0 and sum(self.item.bidvol[0:10]) > 0:
            ratio = int(sum(self.item.offervol[0:10]) / sum(self.item.bidvol[0:10]) * 100) / 100
        return ratio
    def get_cur_price(self):
        return self.item.cur
    def cur_max_price(self, name):
        tmp_df = pd.read_sql("SELECT * FROM %s" % name, self.hoga_db)
        tmp_df.append(self.stock_df.loc[self.stock_df['name']==name])
        max_val = max(tmp_df['cur'])
        print('MAX PRICE : ', max_val)
        return max_val
    def get_stock_name(self, stock_code):
        stock_name = self.stock_df.loc[self.stock_df['code']==stock_code]['name'].iloc[0]
    def strategy1(self, stock_name, cur_price, ground_price, floor_price):
        print('Start Strategy#1')
        # vol_ratio = self.vol_ratio_sell_buy()
        # print('vol_ratio : ', vol_ratio)
        # if vol_ratio < 0.5:
        #     if floor_price < cur_price :
        #         print('<<Strategy BUY : {}>>'.format(stock_name))
        #         self.budget -= (cur_price * 10) #BUY
        #         if self.budget < 0: self.budget += (cur_price * 10) #Cancel
        # elif vol_ratio > 1.5 :
        #     if floor_price > cur_price :
        #         print('<<Strategy SELL : {}>>'.format(stock_name))
        #         self.budget += (cur_price * 10) #SELL
        #         if self.budget > self.max_budget: self.budget -= (cur_price * 10) #Cancel
        # else : print('Trade nothing.')

if __name__ == '__main__':
    #종목명 가져오기
    FILE_stock_list = 'res/stock_list_test.csv' 
    df_stock_list = pd.read_csv(FILE_stock_list, index_col=0)

    #호가 인스턴스 생성
    import datetime as dt
    cur_time = dt.datetime.now()
    BOT_START_TIME = '{:02}{:02}_{:02}{:02}{:02}'.format(cur_time.month, cur_time.day, cur_time.hour, cur_time.minute, cur_time.second)
    obj_hoga = hoga('./db/hoga_{}.db'.format(BOT_START_TIME))
    # print(obj_hoga.stock_df)

    # obj_hoga.db_tbl_list()
    # obj_hoga.db_tbl_delete('세방우')
    # obj_hoga.db_tbl_list()
#0 호가정보 Table Make
    for i in range(len(df_stock_list)):
        stock_code = df_stock_list['code'].values[i]#[:-3]
        print(stock_code)
        #호가 정보 가져오기
        obj_hoga.req_hoga(stock_code)
        print('Vol_Offer / Vol_Bid) : ', obj_hoga.vol_ratio_sell_buy())

        #호가 정보 저장
        obj_hoga.add_10hoga('stgName_None')

        #Invest Strategy
        obj_hoga.strategy1(obj_hoga.get_stock_name(stock_code), obj_hoga.get_cur_price, 0, 9999999)

#DB 업데이트
    for i in range(len(obj_hoga.stock_df)):
        stock_name = obj_hoga.stock_df.iloc[i]['name']
        print(stock_name)
        obj_hoga.db_update(stock_name)
        obj_hoga.db_show(stock_name)

#1 전략 리스트 조회 예제
    # obj8537 = Cp8537()  
    # data8537 = {}
    # obj8537.Clear()
    # ret, data8537 = obj8537.requestList('내 전략') #내 전략, 예제

    # # 1: 기존 감시 중단 (중요)
    # # 종목검색 실시간 감시 개수 제한이 있어, 불필요한 감시는 중단이 필요
    # obj8537.Clear()

    # # 2 - 종목검색 조회: CpSysDib.CssStgFind
    # stgName = 'M20_over_M60_Buy' #on_fixed_vi' #캔들 연속음봉(10분)'
    # item = data8537[stgName]
    # id = item['ID']
    # name = item['전략명']

    # ret, dataStg = obj8537.requestStgID(id)
    # if ret == False :
    #     print('Search No Result')

    # print('검색전략:', id, '전략명:', name, '검색종목수:', len(dataStg))
    # df_stock_list = pd.DataFrame([], columns=['code', 'name'])
    # for item in dataStg:
    #     item['name'] = item.pop('종목명')
    #     print(item)
    #     df_stock_list = df_stock_list.append(item, ignore_index=True)
    
    # for i in range(len(df_stock_list)):
    #     obj_hoga.req_hoga(df_stock_list.iloc[i]['name'])
    #     obj_hoga.add_10hoga()
    #     print(obj_hoga.stock_df[['name', 'offer[0]', 'cur', 'bid[9]']].iloc[-1].values)
