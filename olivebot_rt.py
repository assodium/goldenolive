'''
0) '전략'작성을 통해 'VI'발동 종목 리스트를 가져온다
    - 거래량이 제일 큰 종목을 1-Pick
1) 호가정보 가져오기
2) 매매
    if VI 가격 위에 시가가 있다면,
        Sum(10호가 매도잔량)” / Sum(10호가 매수잔량)” > 1.5
            “ = 최대값 제외
        --> 매수 진행

        Sum(10호가 매도잔량)” / Sum(10호가 매수잔량)” < 1
            “ = 최대값 제외
        --> 매도 진행
'''
TESTMODE = False
import pandas as pd
import datetime as dt
import time
import os
from manager import messenger
messenger = messenger()

tbl_weekday = ['MON','TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN']
cur_time = dt.datetime.now()
BOT_START_TIME = '{:02}{:02}_{:02}{:02}{:02}'.format(cur_time.month, cur_time.day, cur_time.hour, cur_time.minute, cur_time.second)
#1) 호가 Instance Create
import cl_hoga_rt
obj_hoga = cl_hoga_rt.hoga('./db/hoga_{}.db'.format(BOT_START_TIME))

#2) Choose the target company
#VI종목 가져오기
# FILE_stock_list = 'res/stock_list_test.csv' 
# df_stock_list = pd.read_csv(FILE_stock_list, index_col=0)
# print(df_stock_list)

# stock_codes = []
# for i in df_stock_list['code'].values:
#     stock_codes.append(i[:6])

obj8537 = cl_hoga_rt.Cp8537()  
data8537 = {}

#3) 전략 리스트 조회
obj8537.Clear()
ret, data8537 = obj8537.requestList('내 전략') #내 전략, 예제
# for k, v in data8537.items():
#     print(k)

#4) 전략 이용한 종목 조회
# 1: 기존 감시 중단 (중요)
# 종목검색 실시간 감시 개수 제한이 있어, 불필요한 감시는 중단이 필요
obj8537.Clear()

#5) - 종목검색 조회: CpSysDib.CssStgFind
stg = ['on_dyna_vi', 'on_fixed_vi'] #on_fixed_vi up_revenue up_pat1 on_dyna_vi #캔들 연속음봉(10분)'
stgId = [ data8537[stg[0]]['ID'], data8537[stg[1]]['ID'] ]
stgName = [ data8537[stg[0]]['전략명'], data8537[stg[1]]['전략명'] ]

df_stock_list = pd.DataFrame([], columns=['code', 'name', 'stgName'])

try:
    #2) Trader in Action
    messenger.send_slack_msg('#ras_news', 'OliveBot in Action : {}'.format(BOT_START_TIME))
    logfile = open('log_'+BOT_START_TIME+'.txt', 'w')
    prev_hour = 0
    prev_minute = 0
    while True:
        #1. Get on focus stocks
        for i in range(len(stg)):
            ret, dataStg = obj8537.requestStgID(stgId[i]) #stragegy id
            if ret == False :
                print('Search No Result')

            print('검색전략:', stgId[i], '전략명:', stgName[i], '검색종목수:', len(dataStg))
            for item in dataStg:
                item['name'] = item.pop('종목명')
                item['stgName'] = stgName[i]
                print(item)
                bAddStock = True
                # Do not add same stock from 'df_stock_list' 
                for j in range(len(df_stock_list)):
                    if df_stock_list.iloc[j]['name'] == item['name']:
                        bAddStock = False
                        break
                if bAddStock : df_stock_list = df_stock_list.append(item, ignore_index=True)

        time.sleep(20)

        #2. Get 10 Hoga
        if len(df_stock_list) > 0:
            print('Monitored Stock : ', len(df_stock_list))
            if len(df_stock_list) > 5: #30:
                messenger.send_slack_msg('#ras_news', 'Reduce the number of monitoring stock')
                trash_index = df_stock_list.iloc[:2].index
                df_stock_list.drop(trash_index, inplace=True)
                if TESTMODE : print('\n\nReduced List : ', df_stock_list)
            cur_time = dt.datetime.now()
            if TESTMODE : cur_time = dt.datetime(2022,7,8,9,14,10,1)
            diff_minute_from_open_market = (8 - cur_time.hour)*60 + (60 - cur_time.minute)
            cur_weekday = dt.datetime.today().weekday()
            if TESTMODE : print(tbl_weekday[cur_weekday], cur_time)
            #####################<START OF CRITICAL>######################
            if cur_weekday < 5:
                if diff_minute_from_open_market > 0:
                    print('After {}, Market will Open'.format(diff_minute_from_open_market))
                    time.sleep((diff_minute_from_open_market-1)*60)

                elif (cur_time.hour >= 7) and \
                    (cur_time.hour <= 16):
                    for i in range(len(df_stock_list)):
                        obj_hoga.req_hoga(df_stock_list.iloc[i]['code'])
                        logfile.write('[Q] {} : {} - '.format(i, df_stock_list.iloc[i]['name']))
                        obj_hoga.add_10hoga(df_stock_list.iloc[i]['stgName'])
                        logfile.write('[A],  ')
                        #전략 종목 리스트 표시
                        if len(obj_hoga.stock_df) > 0:
                            print(obj_hoga.stock_df[['name', 'offer[0]', 'cur', 'bid[9]']].iloc[-1].values)

                    # MSG Send at every 10 min
                    if cur_time.minute%10 != 0 and prev_minute != cur_time.minute: #Every 10 min, Send msg
                        if TESTMODE : print('MSG send mode')
                        msg = '{} hour : '.format(cur_time.hour)
                        for k in range(len(df_stock_list)):
                            msg = msg + df_stock_list.iloc[k]['name'] + ', '
                        messenger.send_slack_msg('#ras_news', msg)
                        prev_minute = cur_time.minute

                    # DB FLUSH at every 35 min
                    if cur_time.minute%35 != 0 and prev_minute != cur_time.minute: #Every 35 min, Send msg
                        if TESTMODE : print('DB FLUSH mode')
                        messenger.send_slack_msg('#ras_news', 'DB FLUSH')
                        for i in range(len(df_stock_list)):
                            print(df_stock_list.iloc[i]['name'])
                            obj_hoga.db_update(df_stock_list.iloc[i]['code'])
                            obj_hoga.df_clean()
                        prev_minute = cur_time.minute
                        
                    #Trader do strategy
                    # print('Vol rate b/w sell/buy = ', obj_hoga.vol_ratio_sell_buy())

                else:
                    print('Market Close')
                    messenger.send_slack_msg('#ras_news', 'Market Close, Go PC Shutdown')
                    os.system('shutdown -s -t 120 -f')
                    if TESTMODE : os.system('shutdown -a') #OS shutdown Cancel
                    break
                #if time
            else:
                print('This is weekend')
                messenger.send_slack_msg('#ras_news', 'This is Weekend')
                break
            prev_hour = cur_time.hour
            prev_minute = cur_time.minute
            #if cur_weekday
        # if len

    print('OliveBot Ended')
    logfile.close()
    messenger.send_slack_msg('#ras_news', 'OliveBot Ended, PC ll shutdown')
    for i in range(len(df_stock_list)):
        obj_hoga.db_update(df_stock_list.iloc[i]['code'])
    obj_hoga.db_close()
    os.system('shutdown -s -t 120 -f')

except KeyboardInterrupt:
    print('OliveBot KBD Exception')
    logfile.close()
    for i in range(len(df_stock_list)):
        obj_hoga.db_update(df_stock_list.iloc[i]['code'])
    obj_hoga.db_close()

except : 
    print('OliveBot Exception')
    logfile.close()
    
    messenger.send_slack_msg('#ras_news', 'OliveBot Exception, PC ll Reboot')
    msg = '<Stock_List>\n'
    for k in range(len(df_stock_list)):
        # print(df_stock_list.iloc[k]['name'])
        msg = msg + df_stock_list.iloc[k]['name'] + ', '
    msg += '\n<End of MSG>'
    messenger.send_slack_msg('#ras_news', msg)

    print(df_stock_list)
    print(obj_hoga.stock_df)

    for k in range(len(df_stock_list)):
        obj_hoga.db_update(df_stock_list.iloc[k]['code'])
    obj_hoga.db_close()

    # os.system('shutdown -s -t 120 -f')
    os.system('shutdown -r -f -t 120')
    if TESTMODE : os.system('shutdown -a') #OS shutdown Cancel