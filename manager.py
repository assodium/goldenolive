TEST_MODE = False

import os
import datetime
import time
import numpy as np
from slacker import Slacker # OLD API, deprecated
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
import urllib.request
from bs4 import BeautifulSoup
import pandas as pd
from selenium.webdriver.support.ui import WebDriverWait
from selenium import webdriver # python3 -m pip install selenium
        # from selenium.webdriver.support import expected_conditions as EC
        # from selenium.webdriver.common.by import By
class stock_manager:
    def __init__(self):
        self.markettbl_db_name = 'db_markettbl_210716.pkl'
        self.price_db_name = 'db_price_210625.pkl'
        self.path = os.path.dirname(os.path.realpath(__file__)) + '/resources/'
        self.file_extension = '.csv'
        self.oneshot_list = self.path + 'oneshot_list' + self.file_extension
        self.fav_ko_stock_list = self.path + 'stock_list_ko' + self.file_extension
        self.fav_us_stock_list = self.path + 'stock_list_us' + self.file_extension
        self.test_stock_list = self.path + 'stock_list_test' + self.file_extension
        self.idx_list = self.path + 'idx_list' + self.file_extension
        self.markettbl_db_file = self.path + self.markettbl_db_name
        self.price_db_file = self.path + self.price_db_name

        self.db_filter = ['fiftyTwoWeekLow', 'fiftyTwoWeekHigh','trailingEps', 'forwardEps', \
                            'previousClose', 'dividendRate', 'foreign_tbl', 'company_tbl']
        self.db_filter_expansion = ['name', 'price', 'ma60', 'ma120', 'time']
        self.oneshot_chart_path = os.path.dirname(os.path.realpath(__file__)) + '/my_web/static/images/oneshot/'
        self.charts_pic_path = os.path.dirname(os.path.realpath(__file__)) + '/my_web/static/images/every/'
        self.idx_pic_path = os.path.dirname(os.path.realpath(__file__)) + '/my_web/static/images/'

class messenger:
    def __init__(self):
        f = open('./private/token_slack', 'r')
        SLACK_BOT_TOKEN = f.readline()
        self.slack = WebClient(token=SLACK_BOT_TOKEN)
        self.path = os.path.dirname(os.path.realpath(__file__)) + '/resources/'
        self.schedule_csv = self.path + 'family_schedule.csv'
        self.test_schedule_csv = self.path + 'family_schedule_test.csv'

    def send_slack_msg(self, rcver, msg):
        self.slack.chat_postMessage(channel = rcver, text = msg)       

class news:
    def __init__(self):
        path = os.path.dirname(os.path.realpath(__file__)) + '/resources/'
        self.csv = path + '/news_list.csv'
        self.test_csv = path + '/news_list_test.csv'
        self.user_agent = {'User-Agent':'Chrome/66.0.3359.181'} #Avoid HTTP server fobbiden

    def crawling(self, name, url, rule):
        reqUrl = urllib.request.Request(url, headers=self.user_agent)
        fp = urllib.request.urlopen(reqUrl)
        if fp.status == 200:
            source = fp.read()
            fp.close()
            soup = BeautifulSoup(source, 'html.parser')
            soup_found = soup.select(rule)

        out_str = ''
        for idx, str in enumerate(soup_found[:-1]):
            tmp_str = str.get_text()
            tmp_str = tmp_str.replace('\t', '')
            tmp_str = tmp_str.replace('\n', '')
            print('{}# {}'.format(idx, tmp_str))
            out_str = out_str + f'{idx}' + ' ' + tmp_str + '\n'

        cur_time = datetime.datetime.now()
        msg_time = '{}/{} {}:{}'.format(cur_time.month, cur_time.day, cur_time.hour, cur_time.minute)
        msg_packet = msg_time+' : '+name + '\n'+out_str + '\n' + url

        return msg_packet 

class util:
    def __init__(self):
        pass

    def get_datetime(self):
        now_is = datetime.datetime.now()
        cur_date = str(now_is.year) + '-' + str(now_is.month) + '-' + str(now_is.day)
        return cur_date
    
    def get_weekday_hour_min(self):
        now_is = datetime.datetime.now()
        return now_is.weekday(), now_is.hour, now_is.minute
    
    def set_timer(self, set_hour, set_min, timer_name, POLLING_PERIOD):
        while True:
            nowWeekday, nowHour, nowMin = self.get_weekday_hour_min()
            if set_hour <= nowHour:
                if set_min <= nowMin:
                    print('Timer @ manager class : Out')
                    return nowWeekday, nowHour, nowMin
            remaining_time = np.round((((set_hour*60+set_min)-(nowHour*60+nowMin))/60),2)
            print(timer_name, '@ Timer : remain {} hrs'.format(remaining_time))
            time.sleep(POLLING_PERIOD)
    
    def plog(self, LOG_NAME, msg):
        print('[{}] {}'.format(LOG_NAME, msg))

class naver_stock:
    def __init__(self):
        # Chrome Driver
        # DRIVER = os.path.dirname(os.path.realpath(__file__)) + '/resources/chromedriver'
        # options = webdriver.ChromeOptions()
        # options.add_argument('headless')
        # options.add_argument('window-size=640x480')
        # options.add_argument('user-agent=Mozilla/5.0 (X11; CrOS armv7l 13597.84.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36')

        # self.browser=webdriver.Chrome(DRIVER, options=options) # uncomment this line for raspbian

        # Firefox Driver
        import subprocess
#         from pyvirtualdisplay import Display
#         self.display = Display(visible=0, size=(800, 600))
#         self.display.start()
        this_arch = subprocess.Popen('arch', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        chk_arck, err = this_arch.communicate()
        chk_arck = chk_arck.decode('UTF-8').replace('\n','')
        LOG_PATH = os.path.dirname(os.path.realpath(__file__)) + '/resources/web_driver/geckodriver.log'
        if chk_arck == 'x86_64':
            DRIVER = os.path.dirname(os.path.realpath(__file__)) + '/resources/web_driver/firefox_pc/geckodriver'
        else:
            DRIVER = os.path.dirname(os.path.realpath(__file__)) + '/resources/web_driver/firefox_raspi/geckodriver'
            
        firefox_options = webdriver.firefox.options.Options()
        firefox_options.set_headless()
        self.browser = webdriver.Firefox(executable_path=DRIVER, service_log_path=LOG_PATH, firefox_options=firefox_options)
#         self.browser = webdriver.Firefox(executable_path=DRIVER, service_log_path=LOG_PATH)
        
    def destroy(self):
        self.browser.quit()
#         self.display.stop()
        
    def get_nstock_info(self, stock_code):
        # url='https://finance.naver.com/item/frgn.nhn?code=005930'
        url='https://finance.naver.com/item/frgn.nhn?code=' + stock_code
        print('Grab from : ', url)
        self.browser.get(url)
        wait = WebDriverWait(self.browser, 5)
        # print(self.browser)
    #1. Get History
        depth1=self.browser.find_element_by_class_name('content_wrap')
        depth2=depth1.find_element_by_id('content')
        depth3=depth2.find_elements_by_tag_name('table')

        result = depth3[2].text.split('\n')
        result[2] = result[1] + ' ' + result[2].split(' ')[2] + ' ' + result[2].split(' ')[3]

        result2 = []
        for ele in result[2:]:
            ele = ele.replace(',','')
            ele = ele.replace(' ',',')
            # print(ele)
            result2.append(ele)

        df = pd.DataFrame([], columns=result2[0].split(','))
        for idx in range(1,len(result2)):
            df.loc[idx] = result2[idx].split(',')

        df = df.set_index('날짜')
        df = df.sort_index()
        df['종가'] = pd.to_numeric(df['종가'])
        df['외국인'] = pd.to_numeric(df['외국인'])
        df['기관'] = pd.to_numeric(df['기관'])

    #2. Make direction table
        val_list = df['외국인'].iloc[-15:].values #15 days
        direction_tbl_foreign = self.draw_direction_tbl(val_list)
        val_list = df['기관'].iloc[-15:].values #15 days
        direction_tbl_company = self.draw_direction_tbl(val_list)

    #3. Get EPS, FORWARD EPS, DIVIDEND
        ret_dict = {
            'fiftyTwoWeekLow':None,
            'fiftyTwoWeekHigh':None,
            'trailingEps':None,
            'forwardEps':None,
            'previousClose':None,
            'dividendRate':None,
            'foreign_tbl':None,
            'company_tbl':None
        }
        depth1=self.browser.find_element_by_id('aside')
        depth2=depth1.find_element_by_class_name('aside_invest_info')
        depth3=depth2.find_elements_by_tag_name('table')

        if TEST_MODE:
            for id, ele in enumerate(depth3):
                print(id, ' >> ', ele.text+'\n\n')

        len_2nd_txt = len(depth3[2].text.split('\n')[2])
        if len_2nd_txt > 10: # def.26
            print('Normal : ', len_2nd_txt) if TEST_MODE else None
            ret_dict['fiftyTwoWeekLow'] = float(depth3[2].text.split('\n')[2].split(' l ')[1].replace(',', ''))
            ret_dict['fiftyTwoWeekHigh'] = float(depth3[2].text.split('\n')[2].split(' l ')[0].split(' ')[-1].replace(',', ''))
        else : # == def.8
            print('Naver Ryu : ', len_2nd_txt) if TEST_MODE else None
            ret_dict['fiftyTwoWeekLow'] = float(depth3[2].text.split('\n')[3].split(' l ')[1].replace(',', ''))
            ret_dict['fiftyTwoWeekHigh'] = float(depth3[2].text.split('\n')[3].split(' l ')[0].split(' ')[-1].replace(',', ''))
        #1) trailingEps
        print("=trailingEps=", depth3[3].text.split('\n')[2].split(' l ')[1]) if TEST_MODE else None
        if depth3[3].text.split('\n')[2].split(' l ')[1] == 'N/A':
            ret_dict['trailingEps'] = 0
        else:   
            ret_dict['trailingEps'] = float(depth3[3].text.split('\n')[2].split(' l ')[1].replace('원', '').replace(',',''))
        
        #2) forwardEps
        print("=forwardEps=", depth3[3].text.split('\n')[4].split(' l ')[1]) if TEST_MODE else None
        if depth3[3].text.split('\n')[4].split(' l ')[1] == 'N/A':
            ret_dict['forwardEps'] = 0
        else:    
            ret_dict['forwardEps'] = float(depth3[3].text.split('\n')[4].split(' l ')[1].replace('원', '').replace(',',''))
        
        #3) previousClose
        ret_dict['previousClose'] = df['종가'].iloc[-2]
        
        #4) dividendRate
        tmp_str_dividendRate = depth3[3].text.split('\n')[8].split('%')[0]
        if tmp_str_dividendRate == 'N/A':
            ret_dict['dividendRate'] = 0
        else:
            ret_dict['dividendRate'] = float(tmp_str_dividendRate)
        ret_dict['foreign_tbl'] = direction_tbl_foreign
        ret_dict['company_tbl'] = direction_tbl_company
        
        print(ret_dict)

        return df, ret_dict
    
    def draw_direction_tbl(self, values):
        direction_tbl = []
        for ele in values:
            if ele > 0 :
                direction_tbl.append('+')
            else:
                direction_tbl.append('-')
        return ''.join(direction_tbl)


if __name__=='__main__':
    nstock = naver_stock()

    df, ret_dict = nstock.get_nstock_info('361610') #207940 035420
    # print(ret_dict)
    nstock.destroy()

#     import subprocess 
#     cmd = subprocess.Popen('arch', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
#     output, err = cmd.communicate()