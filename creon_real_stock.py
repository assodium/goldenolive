"""
    실시간 체결창 data 받기 구현
"""
from PyQt5.QtCore import pyqtSlot, pyqtSignal, QObject, Qt
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

import win32com.client

from creon_stockcode_codemgr import Creon_CodeMgr

class Creon_StockCur(QObject):
    """
    실시간 체결창 data 받기 구현
    """
    receive_real_stockcur_signal = pyqtSignal(tuple)

    def __init__(self):
        super().__init__()

    def get_real(self, stockcode="005930"):
        creon_stockcode = 'A' + stockcode  # 대신은 주식 종목코드가 'A'로 시작해야함.
        self.SetInputValue(0, creon_stockcode)  # 0 - (string) 종목 코드
        self.Subscribe()

    def stop_real(self, stockcode: str):
        creon_stockcode = 'A' + stockcode  # 대신은 주식 종목코드가 'A'로 시작해야함.
        self.SetInputValue(0, creon_stockcode)  # 0 - (string) 종목 코드
        self.Unsubscribe()

    def OnReceived(self):
        code = self.GetHeaderValue(0)  # 0 - (string) 종목코드
        stockcode = code[1:]  # 'A' 제외시킴.
        ttime = self.GetHeaderValue(18)  # 18 - (long) 시간 (초)
        sell_price = self.GetHeaderValue(7)  # 7 - (long) 매도호가
        buy_price = self.GetHeaderValue(8)  # 8 - (long) 매수호가
        price = self.GetHeaderValue(13)  # 13 - (long) 현재가 또는 예상체결가 ( 19번 예상체결가 구분 플래그에 따라 달라짐)
        predict_price = chr(self.GetHeaderValue(19))  # 19 - (char)예상체결가 구분플래그 ; '1' -> 동시호가시간(예상체결가), '2' -> 장중 (체결)

        state = chr(self.GetHeaderValue(14))  # 14 - (char)체결상태 ; '1' -> 매수, '2' -> 매도
        vol = self.GetHeaderValue(9)  # 9 - (long) 누적거래량  [주의] 기준단위를확인하세요
        vol2 = self.GetHeaderValue(17)  # 17 - (long) 순간체결수량
        money = self.GetHeaderValue(10)  # 10 - (long) 누적거래대금[주의] 기준단위를확인하세요

        pos = chr(self.GetHeaderValue(26))  # 26 - (char)체결상태 (호가방식) - 1.매수/ 2.매도
        vol_sell = self.GetHeaderValue(27)  # 27 - (long) 누적매도체결수량 (호가방식)
        vol_buy = self.GetHeaderValue(28)  # 28 - (long) 누적매수체결수량 (호가방식)
        jang = chr(self.GetHeaderValue(20))  # 20 - (char)장구분플래그;  '2'->장중, '2'->장후시간외 '5'->장후예상체결
        diff = self.GetHeaderValue(2)  # 전일대비
        op = self.GetHeaderValue(4)  # 시가
        hi = self.GetHeaderValue(5)  # 고가
        lo = self.GetHeaderValue(6)  # 저가
        if pos == '1':
            hoga_sign = '+'
        else:
            hoga_sign = '-'
        tick = (stockcode, ttime, sell_price, buy_price, price, state, pos, hoga_sign, vol2, vol, vol_sell, vol_buy, money, jang)

        self.receive_real_stockcur_signal.emit(tick)
        print("---- self.receive_real_stockcur_signal.emit---")

    @classmethod
    def get_instance(cls):
        cp_cur = win32com.client.DispatchWithEvents("dscbo1.StockCur", cls)
        return cp_cur

    if __name__ == "__main__":  
        import sys
  
        class MyMain(QWidget):
            def __init__(self):
                super().__init__()
                self.__realstock = []  # real data 수신중인 종목 리스트
                self.qtxt = QTextEdit(self)
                hbox = QHBoxLayout()
                label = QLabel("종목코드")
                self.lineed = QLineEdit("000660", self)
                hspacer = QSpacerItem(150, 10, QSizePolicy.Expanding)  # 공백 추가 widget
                self.btn = QPushButton("real 추가")
                self.btn2 = QPushButton("real 중단")
                self.btn3 = QPushButton("모든 종목 real 중단")
                hbox.addWidget(label)
                hbox.addWidget(self.lineed)
                hbox.addSpacerItem(hspacer)
                hbox.addWidget(self.btn)
                hbox.addWidget(self.btn2)
                hbox.addWidget(self.btn3)
                vbox = QVBoxLayout()
                vbox.addWidget(self.qtxt)
                vbox.addLayout(hbox)
                self.setLayout(vbox)
                self.setGeometry(700, 50, 600, 900)
                self.btn.clicked.connect(self.__btn_clicked)
                self.btn2.clicked.connect(self.__btn2_clicked)   
                self.btn3.clicked.connect(self.__btn3_clicked)
                self.stockcur = Creon_StockCur.get_instance()
                self.stockcur.receive_real_stockcur_signal.connect(self.__receive_real_stockcur)
                self.cp_codemgr = Creon_CodeMgr()
                self.setWindowTitle("creon plus 체결창 real data 가져오기 -- StockCur 사용")
                self.show()

            @pyqtSlot()
            def __btn_clicked(self):
                # print("종목추가..")
                stockcode = self.lineed.text()
                stockname = self.cp_codemgr.get_code_to_name(stockcode)
                if stockcode in self.__realstock:
                    self.qtxt.append("---- 이미 real data 수신중인 종목입니다. ---{0}  {1}--".format(stockcode, stockname))
                    self.qtxt.append(" ==== real data 수신중인 종목 목록 ====")
                    self.qtxt.append(str(self.__realstock))
                else:
                    self.stockcur.get_real(stockcode)
                    self.qtxt.append("----- 종목 추가 -- {0}  {1} -------".format(stockcode, stockname))
                    self.__realstock.append(stockcode)
                    self.qtxt.append(" ==== real data 수신중인 종목 목록 ====")
                    self.qtxt.append(str(self.__realstock))
 
            @pyqtSlot()
            def __btn2_clicked(self):
                """
                현재 실시간 data 수신중인 목록중에서. 종목코드 항목을 수신 중단함.
                """
                stockcode = self.lineed.text()
                if stockcode in self.__realstock:
                    self.stockcur.stop_real(stockcode)
                    self.__realstock.remove(stockcode)  # 실시간 종목 목록에서 삭제
                    stockname = self.cp_codemgr.get_code_to_name(stockcode)
                    self.qtxt.append("----- 종목 삭제 -- {0}  {1} -------".format(stockcode, stockname))
                    self.qtxt.append(" ==== 삭제후 real data 수신중인 종목 목록 ====")
                    self.qtxt.append(str(self.__realstock))
                else:
                    stockname = self.cp_codemgr.get_code_to_name(stockcode)
                    self.qtxt.append("----- 없는 종목 입니다. -- {0}  {1} -------".format(stockcode, stockname))
                    return

            @pyqtSlot()
            def __btn3_clicked(self):
                """
                수신중인 실시간 data 모두 중단함.
                """
                self.qtxt.append("================> 전체 종목 삭제하기 <=================")
                for stockcode in self.__realstock:
                    self.stockcur.stop_real(stockcode)
                    stockname = self.cp_codemgr.get_code_to_name(stockcode)
                    self.qtxt.append("----- 종목 삭제 -- {0}  {1} -------".format(stockcode, stockname))
                self.__realstock = []  # 초기화

            @pyqtSlot(tuple)
            def __receive_real_stockcur(self, tick: tuple):
                self.qtxt.append(str(tick))

        app = QApplication(sys.argv)
        mywin = MyMain()
        app.exec_()