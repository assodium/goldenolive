from typing import List, Tuple

from PyQt5.QtCore import pyqtSlot, pyqtSignal, QObject, QThread, QTime, QEventLoop, Qt
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

import win32com.client


class Creon_StockCode(QObject):
    def __init__(self):
        super().__init__()
        self.__instance = win32com.client.Dispatch("CpUtil.CpStockCode")

    def code_to_name(self, stockcode: str ="005930"):
        """
        :param stockcode: 종목코드
        :return: 종목명
        """
        code = 'A' + stockcode  # 대신은 주식 종목코드가 'A'로 시작해야함.
        return self.__instance.CodeToName(code)

    def get_total_stock_count(self) -> int:
        """
        종목 코드 수를 반환한다
        """
        return self.__instance.GetCount()

    def get_data(self, data_type, index):
        """
        해당 인덱스의 종목 데이터를 구한다

        :param data_type:  0 - 종목코드, 1 - 종목명, 2 - FullCode

        :param index: 종목코드 인덱스
        :return:
        """
        return self.__instance.GetData(data_type, index)


class Creon_CodeMgr(QObject):
    def __init__(self):
        super().__init__()
        self.__instance = win32com.client.Dispatch("CpUtil.CpCodeMgr")

    def get_market_starttime(self) -> int:
        """
        당일 장 시작 시간 구하기
        """
        starttime = self.__instance.GetMarketStartTime()   # 장 시작 시간
        # print(starttime, type(starttime))
        return starttime

    def get_market_endtime(self) -> int:
        """
        당일 장 종료 시간 구하기
        """
        endtime = self.__instance.GetMarketEndTime()   # 장 종료 시간
        return endtime

    def get_code_to_name(self, stockcode):
        """
        주식종목에 해당하는 종목명 구하기

        :param stockcode: 주식종목
        :return:
        """
        creon_stockcode = 'A' + stockcode   # 대신은 주식 종목코드가 'A'로 시작해야함.
        return self.__instance.CodeToName(creon_stockcode)

    def get_kospi_stockcodes(self) -> List[str]:
        """
        모든 kospi 종목 코드 구하기
        """
        kospi_list = []
        kospi_tuple = self.__instance.GetStockListByMarket(1)  # 거래소 종목 코드 구하기 -- 'Q' 로 시작되는 ETN 종목 포함됨.

        for creon_stockcode in kospi_tuple:
            if creon_stockcode[0] == 'A':  # creon 에서 주식은 'A'로 시작함.
                stockcode = creon_stockcode[1:]   # 첫글자 'A' 제거하기
                kospi_list.append(stockcode)
            else:
                # print(creon_stockcode)
                continue

        return kospi_list

    def get_kodaq_stockcodes(self):
        """
        모든 kosdaq 종목 코드 구하기
        """
        kosdaq_list = []
        kosdaq_tuple = self.__instance.GetStockListByMarket(2)  # 코스닥 종목 코드 구하기

        for creon_stockcode in kosdaq_tuple:
            if creon_stockcode[0] == 'A':  # creon 에서 주식은 'A'로 시작함.
                stockcode = creon_stockcode[1:]   # 첫글자 'A' 제거하기
                kosdaq_list.append(stockcode)
            else:
                # print(creon_stockcode)
                continue

        return kosdaq_list

    def get_marketkind(self, stockcode):
        """
        주식 종목코드로 코스피, 코스닥 소속여부 알아내기
        """
        creon_stockcode = 'A' + stockcode   # 대신은 주식 종목코드가 'A'로 시작해야함.
        market_no = self.__instance.GetStockMarketKind(creon_stockcode)
        if market_no == 0:
            market = "no_market"
        elif market_no == 1:
            market = "kospi"
        elif market_no == 2:
            market = "kosdaq"
        elif market_no == 3:
            market = "k-otc"  # freeboard
        elif market_no == 4:
            market = "krx"
        elif market_no == 5:
            market = "konex"
        else:
            # print(stockcode, market_no)
            market = "market_error"

        # print("market = ", market, type(market))
        return market

    def get_sangjang_date(self, stockcode: str) -> int:
        """
        주식 상장일 구하기
        """
        creon_stockcode = 'A' + stockcode  # 대신은 주식 종목코드가 'A'로 시작해야함.
        return self.__instance.GetStockListedDate(creon_stockcode)

    def is_stock_credit(self, stockcode: str) -> bool:
        """
        주식 종목의 신용가능 여부 구함
        -- 신용가능종목 == True 반환.
        """
        creon_stockcode = 'A' + stockcode  # 대신은 주식 종목코드가 'A'로 시작해야함.

        if self.__instance.IsStockCreditEnable(creon_stockcode) == 1:
            result = True
        else:
            result = False

        return result

    def get_stock_money_rate(self, stockcode: str) -> int:
        creon_stockcode = 'A' + stockcode  # 대신은 주식 종목코드가 'A'로 시작해야함.
        money_rate = self.__instance.GetStockMarginRate(creon_stockcode)
        # print("증거금율 = ", money_rate, type(money_rate))
        return money_rate

    def get_gamli(self, stockcode: str) -> str:
        """
        감리구분 -- 정상, 주의, 경고, 위험예고, 위험
        """
        creon_stockcode = 'A' + stockcode  # 대신은 주식 종목코드가 'A'로 시작해야함.
        control = self.__instance.GetStockControlKind(creon_stockcode)

        if control == 0:
            result = "정상"
        elif control == 1:
            result = "주의"
        elif control == 2:
            result = "경고"
        elif control == 3:
            result = "위험예고"
        elif control == 4:
            result = "위험"
        else:
            result = "error"

        return result

    def is_kwanli(self, stockcode: str):
        """
        관리종목 여부 구함
        -- 관리종목 == True 반환.
        """
        creon_stockcode = 'A' + stockcode  # 대신은 주식 종목코드가 'A'로 시작해야함.

        if self.__instance.GetStockSupervisionKind(creon_stockcode) == 1:
            result = True  # 관리종목
        else:
            result = False  # 일반종목

        return result

    def get_stock_stop_info(self, stockcode):
        """
        종목의 거래정지 정보 반환
        :param stockcode:
        :return:
        """
        creon_stockcode = 'A' + stockcode  # 대신은 주식 종목코드가 'A'로 시작해야함.
        stop_info = self.__instance.GetStockStatusKind(creon_stockcode)
        if stop_info == 0:
            result = "정상"
        elif stop_info == 1:
            result = "거래정지"
        elif stop_info == 2:
            result = "거래중단"
        else:
            result = "error"  # stop_info == 0
        return result

    def get_stock_section_kind(self, stockcode):
        """
        부구분코드 구하기
        """
        creon_stockcode = 'A' + stockcode  # 대신은 주식 종목코드가 'A'로 시작해야함.
        section_kind = self.__instance.GetStockSectionKind(creon_stockcode)
        if section_kind == 0:
            result = "구분없음"
        elif section_kind == 1:
            result = "주권"
        elif section_kind == 2:
            result = "투자회사"
        elif section_kind == 3:
            result = "부동산투자회사"
        elif section_kind == 4:
            result = "선박투자회사"
        elif section_kind == 5:
            result = "사회간접자본투융자회사"
        elif section_kind == 6:
            result = "주식예탁증서"
        elif section_kind == 7:
            result = "신수인수권증권"
        elif section_kind == 8:
            result = "신주인수권증서"
        elif section_kind == 9:
            result = "주식워런트증권"
        elif section_kind == 10:
            result = "상장지수펀드(ETF)"
        elif section_kind == 11:
            result = "수익증권"
        elif section_kind == 12:
            result = "해외ETF"
        elif section_kind == 13:
            result = "외국주권"
        elif section_kind == 14:
            result = "선물"
        elif section_kind == 15:
            result = "옵션"
        else:
            result = "error"

        return result

    def get_lac_info(self, stockcode: str) -> str:
        """
        락 구분 정보 구하기 -- 권리락, 배당락 etc
        """
        creon_stockcode = 'A' + stockcode  # 대신은 주식 종목코드가 'A'로 시작해야함.
        lac_info = self.__instance.GetStockLacKind(creon_stockcode)
        if lac_info == 0:
            result = "구분없음"
        elif lac_info == 1:
            result = "권리락"
        elif lac_info == 2:
            result = "배당락"
        elif lac_info == 3:
            result = "분배락"
        elif lac_info == 4:
            result = "권배락"
        elif lac_info == 5:
            result = "중간배당락"
        elif lac_info == 6:
            result = "권리중간배당락"
        elif lac_info == 99:
            result = "기타"
        else:
            result = "error_lac"

        return result

    def get_stock_base_price(self, stockcode: str) -> int:
        """
        기준가 구하기 -- 권리락 등으로 인한...
        """
        creon_stockcode = 'A' + stockcode  # 대신은 주식 종목코드가 'A'로 시작해야함.
        return self.__instance.GetStockStdPrice(creon_stockcode)

    def get_stock_prev_close_price(self, stockcode: str) -> int:
        """
        전일 종가 구하기...
        """
        creon_stockcode = 'A' + stockcode  # 대신은 주식 종목코드가 'A'로 시작해야함.
        return self.__instance.GetStockYdClosePrice(creon_stockcode)

    def get_stock_price_change_info(self, stockcode: str) -> str:
        """
        액면분할, 액면병합 정보 제공
        """
        creon_stockcode = 'A' + stockcode  # 대신은 주식 종목코드가 'A'로 시작해야함.
        price_change_info = self.__instance.GetStockParPriceChageType(creon_stockcode)
        if price_change_info == 0:
            result = "해당없음"
        elif price_change_info == 1:
            result = "액면분할"
        elif price_change_info == 2:
            result = "액면병합"
        elif price_change_info == 99:
            result = "기타"
        else:
            result = "error"

        return result

    def get_close_month(self, stockcode: str):
        """
        결산월 구하기...
        """
        creon_stockcode = 'A' + stockcode  # 대신은 주식 종목코드가 'A'로 시작해야함.
        return self.__instance.GetStockFiscalMonth(creon_stockcode)

    def get_kospi_upjong_code_list(self) -> tuple:
        """
        모든 kospi 업종 코드 리스트 구하기
        """
        return self.__instance.GetIndustryList()

    def get_kosdaq_upjong_code_list(self) -> tuple:
        """
        모든 kosdaq 업종 코드 리스트 구하기
        """
        return self.__instance.GetKosdaqIndustry1List()

    def get_upjong_code_to_upjong_name(self, upjong_code):
        """
        업종코드로 업종명 구하기

        :param upjong_code: 업종코드
        :return:
        """
        return self.__instance.GetIndustryName(upjong_code)

    def get_upjong_stockcodes(self, upjong_code: int) -> List[str]:
        """
        업종코드에 해당하는 주식종목 리스트 구히기

        :param upjong_code: 업종코드
        :return:
        """
        stockcodes = []

        upjong_stockcodes = self.__instance.GetGroupCodeList(upjong_code)
        if upjong_stockcodes:
            for creon_stockcode in upjong_stockcodes:
                stockcode = creon_stockcode[1:]   # 첫글자 'A' 제거하기
                stockcodes.append(stockcode)
        return stockcodes

    def get_member_list(self) -> Tuple[str]:
        """
        모든 거래원코드(회원사) 코드리스트 구하기
        """
        return self.__instance.GetMemberList()

    def get_member_name(self, member_code: str):
        """
        거래원코드(회원사) 코드에 해당하는 회원사이름 구하기
        """
        return self.__instance.GetMemberName(member_code)


if __name__ == "__main__":
    import sys

    class MyMain(QWidget):
        def __init__(self):
            super().__init__()
            self.qtxt = QTextEdit(self)
            self._btn = QPushButton("장 시간 및 거래원 목록", self)
            self._btn2 = QPushButton("kospi 정보", self)
            self._btn3 = QPushButton("kosdaq 정보", self)
            self._btn4 = QPushButton("kospi 업종 정보", self)
            self._btn5 = QPushButton("kosdaq 업종 정보", self)
            self._btn6 = QPushButton("전체 종목", self)
            vbox = QVBoxLayout()
            vbox.addWidget(self.qtxt)
            vbox.addWidget(self._btn)
            vbox.addWidget(self._btn2)
            vbox.addWidget(self._btn3)
            vbox.addWidget(self._btn4)
            vbox.addWidget(self._btn5)
            vbox.addWidget(self._btn6)
            self.setLayout(vbox)

            self._btn.clicked.connect(self._btn_clicked)
            self._btn2.clicked.connect(self._btn2_clicked)
            self._btn3.clicked.connect(self._btn3_clicked)
            self._btn4.clicked.connect(self._btn4_clicked)
            self._btn5.clicked.connect(self._btn5_clicked)
            self._btn6.clicked.connect(self._btn6_clicked)
            self.setGeometry(700, 50, 800, 700)

            self.cp_codemgr = Creon_CodeMgr()

        def _btn_clicked(self):
            def __members():
                # 거래원 목록 구하기
                members = self.cp_codemgr.get_member_list()  # 모든 거래원 코드 목록 구하기
                # self.qtxt.append(str(members))
                for member in members:
                    member_name = self.cp_codemgr.get_member_name(member)  # 거래원 이름 구하기
                    self.qtxt.append("거래원 코드 = {0}, 거래원 이름 = {1}".format(member, member_name))
                return

            self.qtxt.clear()
            market_starttime = self.cp_codemgr.get_market_starttime()
            market_endtime = self.cp_codemgr.get_market_endtime()
            self.qtxt.append("장 시작시간 = {0}, 장 종료시간 = {1}".format(market_starttime, market_endtime))

            __members()  # 거래원 목록 구하기

        def _btn2_clicked(self):
            kospi = self.cp_codemgr.get_kospi_stockcodes()  # kospi 종목코드 목록 가져오기
            # print(kospi)
            self.qtxt.clear()
            self.qtxt.append("kospi 종목수 = {0}".format(len(kospi)))
            self.qtxt.append("종목코드, 이름, market, 상장일, 신용여부, 증거금율, 감리, 관리종목여부, 정지여부, section, 락 정보, 기준가, 전일종가, 액면정보, 결산월")

            for stockcode in kospi:
                stockname = self.cp_codemgr.get_code_to_name(stockcode)  # 종목코드에 해당하는 종목명 구하기
                market = self.cp_codemgr.get_marketkind(stockcode)  # kospi, kosdaq 등 종목의 market 구하기.
                sangjang = self.cp_codemgr.get_sangjang_date(stockcode)  # 상장일 구하기
                is_credit = self.cp_codemgr.is_stock_credit(stockcode)  # 신용 여부 구하기  --- True == 신용가능종목
                money_rate = self.cp_codemgr.get_stock_money_rate(stockcode)  # 증거금율 구하기
                gamli = self.cp_codemgr.get_gamli(stockcode)  # 감리구분 구하기 -- 주의, 경고, 위험예고, 위험 종목
                is_kwanli = self.cp_codemgr.is_kwanli(stockcode)  # 관리종목 여부 구하기 -- True == 관리종목
                stop_info = self.cp_codemgr.get_stock_stop_info(stockcode)  # 거래정지 정보 얻기
                section = self.cp_codemgr.get_stock_section_kind(stockcode)  # section 정보 얻기
                lac_info = self.cp_codemgr.get_lac_info(stockcode)  # 락 정보 구하기
                base_price = self.cp_codemgr.get_stock_base_price(stockcode)  # 기준가 구하기 -- 권리락 등으로 인한..
                prev_close_price = self.cp_codemgr.get_stock_prev_close_price(stockcode)  # 전일 종가 구하기...
                price_change_info = self.cp_codemgr.get_stock_price_change_info(stockcode)  # 액면분할, 액면병합 정보구하기..
                close_month = self.cp_codemgr.get_close_month(stockcode)  # 결산월
                # print(stockcode, stockname, market, sangjang, is_credit, money_rate, gamli, is_kwanli)
                # print(type(sangjang), is_credit, type(money_rate), gamli, is_kwanli)
                txt = "{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} {14}". \
                    format(stockcode, stockname, market, sangjang, is_credit, money_rate, gamli, is_kwanli,
                           stop_info, section, lac_info, base_price, prev_close_price, price_change_info, close_month)
                self.qtxt.append(txt)

        def _btn3_clicked(self):
            kosdaq = self.cp_codemgr.get_kodaq_stockcodes()
            # print(kosdaq)
            self.qtxt.clear()
            self.qtxt.append("kosdaq 종목수 = {0}".format(len(kosdaq)))
            self.qtxt.append("종목코드, 이름, market, 상장일, 신용여부, 증거금율, 감리, 관리종목여부, 정지여부, section, 락 정보, 기준가, 전일종가, 액면정보, 결산월")

            for stockcode in kosdaq:
                stockname = self.cp_codemgr.get_code_to_name(stockcode)  # 종목코드에 해당하는 종목명 구하기
                market = self.cp_codemgr.get_marketkind(stockcode)  # kospi, kosdaq 등 종목의 market 구하기.
                sangjang = self.cp_codemgr.get_sangjang_date(stockcode)  # 상장일 구하기
                is_credit = self.cp_codemgr.is_stock_credit(stockcode)  # 신용 여부 구하기  --- True == 신용가능종목
                money_rate = self.cp_codemgr.get_stock_money_rate(stockcode)  # 증거금율 구하기
                gamli = self.cp_codemgr.get_gamli(stockcode)  # 감리구분 구하기 -- 주의, 경고, 위험예고, 위험 종목
                is_kwanli = self.cp_codemgr.is_kwanli(stockcode)  # 관리종목 여부 구하기 -- True == 관리종목
                stop_info = self.cp_codemgr.get_stock_stop_info(stockcode)  # 거래정지 정보 얻기
                section = self.cp_codemgr.get_stock_section_kind(stockcode)  # section 정보 얻기
                lac_info = self.cp_codemgr.get_lac_info(stockcode)  # 락 정보 구하기
                base_price = self.cp_codemgr.get_stock_base_price(stockcode)  # 기준가 구하기 -- 권리락 등으로 인한..
                prev_close_price = self.cp_codemgr.get_stock_prev_close_price(stockcode)  # 전일 종가 구하기...
                price_change_info = self.cp_codemgr.get_stock_price_change_info(stockcode)  # 액면분할, 액면병합 정보구하기..
                close_month = self.cp_codemgr.get_close_month(stockcode)  # 결산월
                # print(stockcode, stockname, market, sangjang, is_credit, money_rate, gamli, is_kwanli)
                # print(type(sangjang), is_credit, type(money_rate), gamli, is_kwanli)
                txt = "{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} {14}". \
                    format(stockcode, stockname, market, sangjang, is_credit, money_rate, gamli, is_kwanli,
                           stop_info, section, lac_info, base_price, prev_close_price, price_change_info, close_month)
                self.qtxt.append(txt)

        def _btn4_clicked(self):
            kospi_upjong = self.cp_codemgr.get_kospi_upjong_code_list()
            self.qtxt.clear()
            self.qtxt.append("kospi 업종코드 목록 == " + str(kospi_upjong))
            # print("kospi 업종코드 목록 == ", kospi_upjong)

            for upjong_code in kospi_upjong:
                upjong_name = self.cp_codemgr.get_upjong_code_to_upjong_name(upjong_code)
                self.qtxt.append(upjong_code + " --> " + upjong_name)
                # print(upjong_code, upjong_name)
                upjong_stockcodes = self.cp_codemgr.get_upjong_stockcodes(int(upjong_code))
                # print(upjong_stockcodes)
                self.qtxt.append(str(upjong_stockcodes))

        def _btn5_clicked(self):
            kosdaq_upjong = self.cp_codemgr.get_kosdaq_upjong_code_list()
            self.qtxt.clear()
            self.qtxt.append("kosdaq 업종코드 목록 == " + str(kosdaq_upjong))
            # print("kosdaq 업종코드 목록 == ", kosdaq_upjong)

            for upjong_code in kosdaq_upjong:
                upjong_name = self.cp_codemgr.get_upjong_code_to_upjong_name(upjong_code)
                self.qtxt.append(upjong_code + " --> " + upjong_name)
                upjong_stockcodes = self.cp_codemgr.get_upjong_stockcodes(int(upjong_code))
                # print(upjong_stockcodes)
                self.qtxt.append(str(upjong_stockcodes))

        def _btn6_clicked(self):
            try:
                cp_stockcode = Creon_StockCode()

                count = cp_stockcode.get_total_stock_count()
                # print(type(count))
                self.qtxt.clear()
                self.qtxt.append("종목코드 개수= {0}".format(count))

                for idx in range(count):
                    code = cp_stockcode.get_data(0, idx)
                    if code[0] == 'A':
                        stockcode = code[1:]   # 주식은 'A'로 시작.
                    else:
                        stockcode = code

                    stockname = cp_stockcode.get_data(1, idx)
                    fullcode = cp_stockcode.get_data(2, idx)
                    txt = "{0} ; 종목코드= {1}, 종목명= {2}, FullCode= {3}".format(idx, stockcode, stockname, fullcode)
                    self.qtxt.append(txt)
            except Exception as e:
                print(e)


    app = QApplication(sys.argv)
    mywin = MyMain()
    mywin.show()

    app.exec_()
