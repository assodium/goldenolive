import sqlite3
import pandas as pd
"""
conn = sqlite3.connect('gwan.db')
c = conn.cursor()
c.execute('CREATE TABLE IF NOT EXISTS user \
    (id INTEGER PRIMARY KEY, username TEXT, email TEXT, phone TEXT \
        , regist_date TEXT)')

import datetime
now = datetime.datetime.now()

# c.execute("INSERT INTO user VALUES \
#     (7, 'Kim', 'kim@naver.com', '010-2515-1212', ?)", (now,))
c.execute("SELECT * FROM user")

print('All -> \n', c.fetchall())

conn.commit()
conn.close()
"""
queries = {
    'tbl_list': "SELECT name FROM sqlite_master WHERE type IN ('table', 'view') AND name NOT LIKE 'sqlite_%' UNION ALL SELECT name FROM sqlite_temp_master WHERE type IN ('table', 'view') ORDER BY 1",
    'get_tbl' : "SELECT * FROM {}" #.format('NextChip')
}

pDB = sqlite3.connect('vi_hoga1.db')
c = pDB.cursor()

tbl_name = 'K003580' #K005380
stock_name = 'HLB생명과학' #HLB

# c.execute(queries['tbl_list'])
c.execute("SELECT * FROM {}".format(tbl_name))
print('All -> \n', c.fetchall())

tbl_name = 'STX중공업'
stock_name = 'Union'
df = pd.read_sql("SELECT * FROM {}".format(tbl_name), pDB)

df['cur_time2'] = 0
for i in range(len(df)):
    df['cur_time'][i] = df['cur_time'][i].replace('/','')
    df['cur_time'][i] = df['cur_time'][i].replace('_','')
    df['cur_time'][i] = df['cur_time'][i].replace(':','')

df['cur_time'] = df['cur_time'].astype('int64')
pdf = df.loc[df['cur_time']>220707000000]
pdf = pdf.loc[pdf['cur_time']<220708153100]

pdf.to_sql(stock_name, pDB, if_exists='append', index=False)

tbl_name = 'K000720'
c.execute("DROP TABLE {}".format(tbl_name))

pDB.commit()

pDB.close()

"""
4. DB Merge
"""
import sqlite3

queries = {
    'tbl_list': "SELECT name FROM sqlite_master WHERE type IN ('table', 'view') AND name NOT LIKE 'sqlite_%' UNION ALL SELECT name FROM sqlite_temp_master WHERE type IN ('table', 'view') ORDER BY 1"
    # 'get_tbl' : "SELECT * FROM {}" #.format('NextChip'),
    # 'tbl_rename' : "alter table {} rename to {}"
    # 'get_cols : SELECT sql FROM sqlite_master WHERE tbl_name='myTable'
}

pDB1 = sqlite3.connect('./db/hoga_0913_075527.db')
pDB2 = sqlite3.connect('./db/hoga_0913_121131.db')
c1 = pDB1.cursor()
c2 = pDB2.cursor()

#Get Table list
c1.execute(queries['tbl_list'])
tbl_list1 = c1.fetchall()
c2.execute(queries['tbl_list'])
tbl_list2 = c2.fetchall()

#Make Overlap Table list
overlap_tbl_list = []
for i in tbl_list1:
    if i in tbl_list2:
        overlap_tbl_list.append(i[0])

#Merge data 
tbl_name = 'ARIRANG 신흥국MSCI인버스(합성 H)'
c1.execute("SELECT * FROM [{}]".format(tbl_name))
q_res1 = c1.fetchall()

c2.execute("INSERT INTO [{}] VALUES {}".format(tbl_name, q_res1))
q_res2 = c2.fetchall()

tbl_name = '(이스타코,)'
c1.execute("SELECT table FROM sqlite_master")
# c1.execute("SELECT name FROM sqlite_master WHERE tbl_name=[{}]".format(tbl_name))
q_res3 = c1.fetchall()

pDB1.commit()
pDB2.commit()

pDB1.close()
pDB2.close()
