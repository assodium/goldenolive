import win32com.client
import pandas as pd
from datetime import date, timedelta

yester = date.today() - timedelta(days=10)
indate = '{:04}{:02}{:02}'.format(yester.year,yester.month,yester.day)

# 연결 여부 체크
objCpCybos = win32com.client.Dispatch("CpUtil.CpCybos")
bConnect = objCpCybos.IsConnect
if (bConnect == 0):
    print("PLUS가 정상적으로 연결되지 않음. ")
    exit()
 
# 차트 객체 구하기
objStockChart = win32com.client.Dispatch("CpSysDib.StockChart")
stockname = '삼성전자'
stockcode = 'A005930' #삼성전자
# stockcode = 'A102280' #쌍방울
objStockChart.SetInputValue(0, stockcode)   #종목 코드 - 삼성전자
objStockChart.SetInputValue(1, ord('1')) # 기간으로 조회
objStockChart.SetInputValue(3, indate) # 날짜입력
objStockChart.SetInputValue(5, [0,1,2,3,4,5,8,9]) #날짜,시간,시가,고가,저가,종가,거래량, 거래대금
objStockChart.SetInputValue(6, ord('m')) # '차트 주가 - 분봉 차트 요청
objStockChart.SetInputValue(7, 15) # 차트 주기 - 1분봉
objStockChart.SetInputValue(9, ord('1')) # 수정주가 사용
    
day_list = []
time_list = []
open_list = []
high_list = []
low_list = []
close_list = []
vol_list = []
amount_list = []


while True : 
    objStockChart.BlockRequest()
    cnt = objStockChart.GetHeaderValue(3) # 3은 요청 개수
    print(cnt) # 한번에 일부의 데이터만 받아올 수 있다! => 한번 더해주고, 받아올 데이터가 있으면 또 BlockRequest해줌
    for i in range(cnt) : # 
        day_list.append(objStockChart.GetDataValue(0, i))
        time_list.append(objStockChart.GetDataValue(1, i))
        open_list.append(objStockChart.GetDataValue(2, i))
        high_list.append(objStockChart.GetDataValue(3, i))
        low_list.append(objStockChart.GetDataValue(4, i))
        close_list.append(objStockChart.GetDataValue(5, i))
        vol_list.append(objStockChart.GetDataValue(6, i))
        amount_list.append(objStockChart.GetDataValue(7, i))
    if objStockChart.Continue == False : # 더 요청할 데이터가 없으면 break, 있으면 계속 요청!
        break 
    
dict1 = {'day' : day_list, 'time' : time_list, 'open' :open_list, 'high' : high_list, 'low' : low_list, 'close' :close_list,
         'vol' : vol_list, 'amount' : amount_list}
df = pd.DataFrame(dict1,
                  columns=['day', 'time', 'open', 'high', 'low', 'close','vol', 'amount']) # 2. 데이터프레임으로 만들기

r_idx = [i for i in range(df.shape[0]-1, -1, -1)]
# df = df.sort_index(ascending=False)
# df.to_csv("{}.csv".format(stockcode), index = False) # csv로 삼성전자 전구간 분봉을 저장
df_r = pd.DataFrame(df, index=r_idx)
df_r = df_r.reset_index(drop=True)
print(df_r)

import matplotlib.pyplot as plt
plt.rc('font', family='NanumGothic') # For Windows
# print(plt.rcParams['font.family'])

plt.suptitle(stockname + indate + '~')
plt.subplot(2,1,1)
plt.plot(df['close'])
plt.subplot(2,1,2)
plt.plot(df['vol'])
plt.axvline(x)

# r_idx = [ i for i in range(df.shape[0]-1, -1, -1)]
# r_idx = [ i for i in range(0, df.shape[0])]
# r_df = pd.DataFrame(df, index=r_idx)
