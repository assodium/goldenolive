from datetime import *
import numpy as np
import pandas as pd
import requests
from ctypes import *

import backtrader as bt
# import yfinance as yf

## error 참조
# https://community.backtrader.com/topic/981/importerror-cannot-import-name-min_per_hour-when-trying-to-plot/7


# from MyDB_lib import *
# MyDB = MyDB()
def test_RSI_trade():
    class firstStrategy(bt.Strategy):

        def __init__(self):
            self.rsi = bt.indicators.RSI_SMA(self.data.close, period=21)

        def next(self):
            if not self.position:
                if self.rsi < 30:  
                    self.buy(size=100)
            else:
                if self.rsi > 70:
                    self.sell(size=100)

    # Variable for our starting cash
    startcash = 10000

    # Create an instance of cerebro
    cerebro = bt.Cerebro()

    # Add our strategy
    cerebro.addstrategy(firstStrategy)

    # Get Apple data from Yahoo Finance.

    data = bt.feeds.Quandl(
        dataname='AAPL',
        fromdate=datetime(2016, 1, 1),
        todate=datetime(2017, 1, 1),
        buffered=True
    )

    # Add the data to Cerebro
    cerebro.adddata(data)

    # Set our desired cash start
    cerebro.broker.setcash(startcash)

    # Run over everything
    
    cerebro.run()

    # Get final portfolio Value
    portvalue = cerebro.broker.getvalue()
    pnl = portvalue - startcash

    # Print out the final result
    print('Final Portfolio Value: ${}'.format(portvalue))
    print('P/L: ${}'.format(pnl))

    # Finally plot the end results
    cerebro.plot(style='candlestick') 

def test_SMA_trade():
    # Create a subclass of Strategy to define the indicators and logic
    class SmaCross(bt.Strategy):
        # list of parameters which are configurable for the strategy
        params = dict(
            pfast=10,  # period for the fast moving average
            pslow=30   # period for the slow moving average
        )

        def __init__(self):
            sma1 = bt.ind.SMA(period=self.p.pfast)  # fast moving average
            sma2 = bt.ind.SMA(period=self.p.pslow)  # slow moving average
            self.crossover = bt.ind.CrossOver(sma1, sma2)  # crossover signal

        def next(self):
            if not self.position:  # not in the market
                if self.crossover > 0:  # if fast crosses slow to the upside
                    self.buy()  # enter long
            elif self.crossover < 0:  # in the market & cross to the downside
                self.close()  # close long position


    cerebro = bt.Cerebro()  # create a "Cerebro" engine instance

    # Create a data feed
    # df = yf.download("000660.KS", start="2011-01-01")
    # data = bt.feeds.PandasData(dataname=df)
    data = bt.feeds.Quandl(dataname='MSFT',
                                     fromdate=datetime(2011, 1, 1),
                                     todate=datetime(2012, 12, 31))

    cerebro.adddata(data)  # Add the data feed
    cerebro.addstrategy(SmaCross)  # Add the trading strategy
    cerebro.run()  # run it all
    cerebro.plot()  # and plot it with a single command 

if __name__ == "__main__":
    # lst_item_code = MyDB.get_api_item('n')  # 'nuh'
    # test_SMA_trade()
    test_RSI_trade()
    # test_comp() 