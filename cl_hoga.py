DEBUG_MODE = False
from pickletools import stringnl
from sys import _current_frames
from numpy import append
# from signal import strsignal
import pandas as pd
import win32com.client
from pandas import Series, DataFrame, read_sql
import locale
import os
import sqlite3
import datetime
import time

# cp object
g_objCodeMgr = win32com.client.Dispatch("CpUtil.CpCodeMgr")
g_objCpStatus = win32com.client.Dispatch("CpUtil.CpCybos")
g_objCpTrade = win32com.client.Dispatch("CpTrade.CpTdUtil")
locale.setlocale(locale.LC_ALL, '')

# 현재가 정보 저장 구조체
class stockPricedData:
    def __init__(self):
        self.dicEx = {ord('0'): "동시호가/장중 아님", ord('1'): "동시호가", ord('2'): "장중"}
        self.code = ""
        self.name = ""
        self.cur = 0        # 현재가
        self.diff = 0       # 대비
        self.diffp = 0      # 대비율
        self.offer = 0     # 매도호가
        self.bid = 0       # 매수호가
        self.offervol = 0     # 매도호가 잔량
        self.bidvol = 0       # 매수호가 잔량
        self.totOffer = 0       # 총매도잔량
        self.totBid = 0         # 총매수 잔량
        self.vol = 0            # 거래량
        self.tvol = 0           # 순간 체결량
        self.baseprice = 0      # 기준가
        self.high = 0
        self.low = 0
        self.open = 0
        self.volFlag = ord('0')  # 체결매도/체결 매수 여부
        self.time = 0
        self.sum_buyvol = 0
        self.sum_sellvol = 0
        self.vol_str = 0        # 체결강도

        self.diff_vol = 0         # 전일거래량
        self.foreign_ratio = 0        # 외국인보유비율
        self.vol_money = 0       # 거래금액
        self.num_stock = 0         # 상장주식수

    def __del__(self):
        print('stockPricedData del()')

# CpRPCurrentPrice:  현재가 기본 정보 조회 클래스
class CpRPCurrentPrice:
    def __init__(self):
        if (g_objCpStatus.IsConnect == 0):
            print("PLUS가 정상적으로 연결되지 않음. ")
            return
        self.objStockMst = win32com.client.Dispatch("DsCbo1.StockMst2")
        return
    def Request(self, code, rtMst, stgName):
        now = datetime.datetime.now()
        cur_date = now.strftime('%y%m%d')
        cur_time = now.strftime('%H%M%S')
        if DEBUG_MODE: print("Data Time: ", cur_time)

        self.objStockMst.SetInputValue(0, code)
        ret = self.objStockMst.BlockRequest()
        if self.objStockMst.GetDibStatus() != 0:
            print("통신상태", self.objStockMst.GetDibStatus(), self.objStockMst.GetDibMsg1())
            return False

        buf_df = pd.DataFrame(columns=['stgName','code', 'name', 'date', 'cur_time', 'cur', 'diff', 'diffp',
                                                'offer', \
                                                'bid', \
                                                'offervol', \
                                                'bidvol', \
                                                'totOffer', 'totBid', 'diff_vol', 'vol', 'tvol', 'vol_str', 'baseprice', 'high', 'low', 'open', 'volFlag', 'time', 'sum_buyvol', 'sum_sellvol', \
                                                'foreign_ratio', 'vol_money', 'num_stock'
                                            ])

        print('Rcv Num of stock : ', self.objStockMst.GetHeaderValue(0))
        new_list = []
        for i in range(self.objStockMst.GetHeaderValue(0)):
            rtMst.code = self.objStockMst.GetDataValue(0, i)
            rtMst.name = self.objStockMst.GetDataValue(1, i)
            if DEBUG_MODE: print('Beside : ', self.objStockMst.GetDataValue(1, i), self.objStockMst.GetDataValue(9, i))
            rtMst.cur =  self.objStockMst.GetDataValue(3, i)  # 종가
            rtMst.diff =  self.objStockMst.GetDataValue(4, i)  # 전일대비
            if rtMst.cur-rtMst.diff != 0:
                rtMst.diffp = int(rtMst.diff/(rtMst.cur-rtMst.diff) * 10000) / 100
            else :
                rtMst.diffp = 9999 
            rtMst.baseprice  =  self.objStockMst.GetDataValue(19, i) # 기준가
            rtMst.vol = self.objStockMst.GetDataValue(11, i)  # 거래량
            rtMst.volFlag = self.objStockMst.GetDataValue(5, i)  # 상태구분
            rtMst.open =  self.objStockMst.GetDataValue(6, i)  # Open
            rtMst.high =  self.objStockMst.GetDataValue(7, i)  # High
            rtMst.low =  self.objStockMst.GetDataValue(8, i)  # Low
            rtMst.vol_str = self.objStockMst.GetDataValue(21, i)  # 체결강도
            rtMst.tvol = self.objStockMst.GetDataValue(22, i)  # 순간체결량

            rtMst.totOffer = self.objStockMst.GetDataValue(13, i)  # 총매도잔량
            rtMst.totBid = self.objStockMst.GetDataValue(14, i)  # 총매수잔량

            rtMst.diff_vol = self.objStockMst.GetDataValue(20, i)
            rtMst.foreign_ratio = self.objStockMst.GetDataValue(18, i)
            rtMst.vol_money = self.objStockMst.GetDataValue(12, i)
            rtMst.num_stock = self.objStockMst.GetDataValue(17, i)
            rtMst.time = self.objStockMst.GetDataValue(2, i)

            rtMst.offer = (self.objStockMst.GetDataValue(9, i))  # 매도호가
            rtMst.bid = (self.objStockMst.GetDataValue(10, i) ) # 매수호가
            rtMst.offervol = (self.objStockMst.GetDataValue(15, i))  # 매도호가 잔량
            rtMst.bidvol = (self.objStockMst.GetDataValue(16, i) ) # 매수호가 잔량

            new_list.append((stgName, rtMst.code, rtMst.name, cur_date, cur_time, rtMst.cur, rtMst.diff, rtMst.diffp, \
                            rtMst.offer, rtMst.bid, rtMst.offervol, rtMst.bidvol, rtMst.totOffer, rtMst.totBid, \
                            rtMst.diff_vol, rtMst.vol, rtMst.tvol, rtMst.vol_str, rtMst.baseprice, rtMst.high, rtMst.low, rtMst.open, \
                            rtMst.volFlag, rtMst.time, rtMst.sum_buyvol, rtMst.sum_sellvol, \
                            rtMst.foreign_ratio, rtMst.vol_money, rtMst.num_stock
                            ))

        new_df = pd.DataFrame(new_list, columns=['stgName','code', 'name', 'date', 'cur_time', 'cur', 'diff', 'diffp',
                                                'offer', \
                                                'bid', \
                                                'offervol', \
                                                'bidvol', \
                                                'totOffer', 'totBid', 'diff_vol', 'vol', 'tvol', 'vol_str', 'baseprice', 'high', 'low', 'open', 'volFlag', 'time', 'sum_buyvol', 'sum_sellvol', \
                                                'foreign_ratio', 'vol_money', 'num_stock'
                                            ])

        # buf_df = buf_df.append(new_df, ignore_index=True)
        print(new_df)
        return new_df

        # if DEBUG_MODE:
        # print('[F:Request] {}, {}, 종가:{}, 매도호가:{}, 매도잔량:{}, 총매도잔량:{}, 매수호가:{}, 매수잔량:{}, 총매수잔량:{}'.format( \
        #     rtMst.code, rtMst.name, rtMst.cur, rtMst.offer, rtMst.offervol, rtMst.totOffer, rtMst.bid, rtMst.bidvol, rtMst.totBid))


# Cp8537 : 종목검색 전략 조회
class Cp8537:
    def __init__(self):
        self.bisSB = False
        self.monList = {}

    def __del__(self):
        self.Clear()

    def Clear(self):
        self.stopAllStgControl()
        if self.bisSB:
            self.bisSB = False

    def requestList(self, sel):
        retStgList = {}
        objRq = win32com.client.Dispatch("CpSysDib.CssStgList")

        # 예제 전략에서 전략 리스트를 가져옵니다.
        if (sel == '예제') :
            objRq.SetInputValue(0, ord('0'))   # '0' : 예제전략, '1': 나의전략
        else :
            objRq.SetInputValue(0, ord('1'))  # '0' : 예제전략, '1': 나의전략
        objRq.BlockRequest()

        # 통신 및 통신 에러 처리
        rqStatus = objRq.GetDibStatus()
        if rqStatus != 0:
            rqRet = objRq.GetDibMsg1()
            print("통신상태", rqStatus, rqRet)
            return (False, retStgList)

        cnt = objRq.GetHeaderValue(0) # 0 - (long) 전략 목록 수
        flag = objRq.GetHeaderValue(1) # 1 - (char) 요청구분
        print('종목검색 전략수:', cnt)


        for i in range(cnt):
            item = {}
            item['전략명'] = objRq.GetDataValue(0, i)
            item['ID'] = objRq.GetDataValue(1, i)
            item['전략등록일시'] = objRq.GetDataValue(2, i)
            item['작성자필명'] = objRq.GetDataValue(3, i)
            item['평균종목수'] = objRq.GetDataValue(4, i)
            item['평균승률'] = objRq.GetDataValue(5, i)
            item['평균수익'] = objRq.GetDataValue(6, i)
            retStgList[item['전략명']] = item
            print(item)
            
        return (True, retStgList)

    def requestStgID(self, id):
        retStgList = []
        objRq = None
        objRq = win32com.client.Dispatch("CpSysDib.CssStgFind")
        objRq.SetInputValue(0, id) # 전략 id 요청
        objRq.BlockRequest()
        # 통신 및 통신 에러 처리
        rqStatus = objRq.GetDibStatus()
        if rqStatus != 0:
            rqRet = objRq.GetDibMsg1()
            print("통신상태", rqStatus, rqRet)
            return (False, retStgList)

        cnt = objRq.GetHeaderValue(0)  # 0 - (long) 검색된 결과 종목 수
        totcnt = objRq.GetHeaderValue(1)  # 1 - (long) 총 검색 종목 수
        stime = objRq.GetHeaderValue(2)  # 2 - (string) 검색시간
        print('검색된 종목수:', cnt, '전체종목수:', totcnt, '검색시간:', stime)

        for i in range(cnt):
            item = {}
            item['code'] = objRq.GetDataValue(0, i)
            item['종목명'] = g_objCodeMgr.CodeToName(item['code'])
            retStgList.append(item)

        return (True, retStgList)

    def requestMonitorID(self, id):
        objRq = win32com.client.Dispatch("CpSysDib.CssWatchStgSubscribe")
        objRq.SetInputValue(0, id) # 전략 id 요청
        objRq.BlockRequest()


        # 통신 및 통신 에러 처리
        rqStatus = objRq.GetDibStatus()
        if rqStatus != 0:
            rqRet = objRq.GetDibMsg1()
            print("통신상태", rqStatus, rqRet)
            return (False, 0)

        monID = objRq.GetHeaderValue(0)
        if  monID == 0:
            print('감시 일련번호 구하기 실패')
            return (False, 0)

        # monID - 전략 감시를 위한 일련번호를 구해온다.
        # 현재 감시되는 전략이 없다면 감시일련번호로 1을 리턴하고,
        # 현재 감시되는 전략이 있다면 각 통신 ID에 대응되는 새로운 일련번호를 리턴한다.
        return (True, monID)

    def requestStgControl(self, id, monID, bStart):
        objRq = win32com.client.Dispatch("CpSysDib.CssWatchStgControl")
        objRq.SetInputValue(0, id) # 전략 id 요청
        objRq.SetInputValue(1, monID)  # 감시일련번호

        if bStart == True:
            objRq.SetInputValue(2, ord('1'))  # 감시시작
            print('전략감시 시작 요청 ', '전략 ID:', id, '감시일련번호',monID)
        else:
            objRq.SetInputValue(2, ord('3'))  # 감시취소
            print('전략감시 취소 요청 ', '전략 ID:', id, '감시일련번호', monID)
        objRq.BlockRequest()


        # 통신 및 통신 에러 처리
        rqStatus = objRq.GetDibStatus()
        if rqStatus != 0:
            rqRet = objRq.GetDibMsg1()
            print("통신상태", rqStatus, rqRet)
            return (False, '')

        status = objRq.GetHeaderValue(0)

        if status == 0 :
            print('전략감시상태: 초기상태')
        elif status == 1 :
            print('전략감시상태: 감시중')
        elif status == 2 :
            print('전략감시상태: 감시중단')
        elif status == 3 :
            print('전략감시상태: 등록취소')

        # event 수신 요청 - 요청 중이 아닌 경우에만 요청
        # if self.bisSB == False:
        #     print('Now going to RT subscribe Mode.. But Today..')
        #     self.objpb.Subscribe('', self) #disable for Test : 8/4
        #     self.bisSB = True

        # 진행 중인 전략들 저장
        if bStart == True:
            self.monList[id] = monID
        else:
            if id in self.monList :
                del self.monList[id]

        return (True, status)

    def stopAllStgControl(self):
        delitem = []
        for id, monId in self.monList.items() :
            delitem.append((id,monId))

        for item in delitem:
            self.requestStgControl(item[0], item[1], False)

        print(len(self.monList))

    def checkRealtimeStg(self, pbData):
        # 감시중인 전략인 경우만 체크
        id = pbData['전략ID']
        monid = pbData['감시일련번호']
        if not (id in self.monList) :
            return

        if (monid != self.monList[id]) :
            return

        print(pbData)

def init_dataframe():
    df = pd.DataFrame(columns=['stgName','code', 'name', 'date', 'cur_time', 'cur', 'diff', 'diffp',
                                                'offer', \
                                                'bid', \
                                                'offervol', \
                                                'bidvol', \
                                                'totOffer', 'totBid', 'diff_vol', 'vol', 'tvol', 'vol_str', 'baseprice', 'high', 'low', 'open', 'volFlag', 'time', 'sum_buyvol', 'sum_sellvol', \
                                                'foreign_ratio', 'vol_money', 'num_stock'
                                            ])
    return df

class hoga:
    def __init__(self, db_file):
        self.budget = 100000000
        self.max_budget = 100000000
        self.objMst = CpRPCurrentPrice()
        self.item = stockPricedData()

        self.stock_df = pd.DataFrame(columns=['stgName','code', 'name', 'date', 'cur_time', 'cur', 'diff', 'diffp',
                                                'offer', \
                                                'bid', \
                                                'offervol', \
                                                'bidvol', \
                                                'totOffer', 'totBid', 'diff_vol', 'vol', 'tvol', 'vol_str', 'baseprice', 'high', 'low', 'open', 'volFlag', 'time', 'sum_buyvol', 'sum_sellvol', \
                                                'foreign_ratio', 'vol_money', 'num_stock'
                                            ])
        self.hoga_db = sqlite3.connect(db_file)
        self.db_driver = self.hoga_db.cursor()
        self.db_queries = {
            'tbl_list': "SELECT name FROM sqlite_master WHERE type IN ('table', 'view') AND name NOT LIKE 'sqlite_%' UNION ALL SELECT name FROM sqlite_temp_master WHERE type IN ('table', 'view') ORDER BY 1",
            'get_tbl' : "SELECT * FROM {}" #.format('NextChip')
        }
    def req_hoga(self, code, stgName):
        new_df = self.objMst.Request(code, self.item, stgName)
        print('SHOW DF : ', new_df)
        return new_df
    def merge_df(self, new_df):
        self.stock_df = self.stock_df.append(new_df, ignore_index=True)
    def db_update(self, name):
        each_df = self.stock_df.loc[self.stock_df['name']==name]
        each_df.to_sql(name, self.hoga_db, if_exists='append', index=False)
        # each_df.to_sql(code, self.hoga_db, if_exists='append', index=False)
        # self.hoga_db.execute('ALTER TABLE 'K005930' ADD PRIMARY KEY ("cur_time");')
        print('DB updated')   
    def db_show(self, name):
        self.db_driver.execute("SELECT * FROM %s" % name)
        print('DB : ', self.db_driver.fetchall())
    def db_tbl_list(self):
        self.db_driver.execute(self.db_queries['tbl_list'])
        print('DB : ', self.db_driver.fetchall())
    def db_tbl_delete(self, tbl_name):
        self.db_driver.execute("DROP TABLE {}".format(tbl_name))
        print('DB Table Drop : {}'.format(tbl_name))
    def df_clean(self):
        self.stock_df = init_dataframe()
        print('Clean : DF')
    def db_close(self):
        self.hoga_db.commit()
        self.hoga_db.close()
        print('DB Close')
    def vol_ratio_sell_buy(self):
        ratio = 1 #def. ratio
        if self.item.offervol > 0 and self.item.bidvol > 0:
            ratio = int(self.item.offervol / self.item.bidvol * 100) / 100
        return ratio
    def get_cur_price(self):
        return self.item.cur
    def cur_max_price(self, name):
        tmp_df = pd.read_sql("SELECT * FROM %s" % name, self.hoga_db)
        tmp_df.append(self.stock_df.loc[self.stock_df['name']==name])
        max_val = max(tmp_df['cur'])
        print('MAX PRICE : ', max_val)
        return max_val
    def get_stock_name(self, stock_code):
        stock_name = self.stock_df.loc[self.stock_df['code']==stock_code]['name'].iloc[0]
    def strategy1(self, cur_price, ground_price, floor_price, vol_ratio):
        print('STRATEGY # vol_ratio={}, cur={}, low={}, high={}'.format(vol_ratio, cur_price, ground_price, floor_price))
        trade_action = ''
        trade_n_item = 0
        if vol_ratio < 1.0 and vol_ratio != 0:
            if floor_price*0.98 < cur_price :
                trade_action = 'BUY'
                trade_n_item = 10
                self.budget -= (cur_price * 10) #BUY
                if self.budget < 0: 
                    self.budget += (cur_price * 10) #Cancel
                    trade_n_item = 0
            else : trade_action = 'NOTHING'
        elif vol_ratio > 1.5 :
            if floor_price*0.98 > cur_price :
                trade_action = 'SELL'
                trade_n_item = 10
                self.budget += (cur_price * 10) #SELL
                if self.budget > self.max_budget: 
                    self.budget -= (cur_price * 10) #Cancel
                    trade_n_item = 0
            else : trade_action = 'NOTHING'
        else : trade_action = 'NOTHING'
        print('Now Budget : {}'.format(self.budget))

        return trade_action, trade_n_item

if __name__ == '__main__':
    #종목명 가져오기
    FILE_stock_list = 'res/stock_list_test.csv' 
    df_stock_list = pd.read_csv(FILE_stock_list, index_col=0)

    #호가 인스턴스 생성
    import datetime as dt
    cur_time = dt.datetime.now()
    BOT_START_TIME = '{:02}{:02}_{:02}{:02}{:02}'.format(cur_time.month, cur_time.day, cur_time.hour, cur_time.minute, cur_time.second)
    obj_hoga = hoga('./db/hoga_{}.db'.format(BOT_START_TIME))
    # print(obj_hoga.stock_df)

    # obj_hoga.db_tbl_list()
    # obj_hoga.db_tbl_delete('세방우')
    # obj_hoga.db_tbl_list()
    stock_string = ''
    for i in range(len(df_stock_list)):
        stock_string += df_stock_list['code'].values[i] + ','
    # stock_string += ','
    print(stock_string)
    cur_df = obj_hoga.req_hoga(stock_string, 'stgName_None')
    print('CUR DF : ', cur_df)
#0 호가정보 Table Make
    # for i in range(len(df_stock_list)):
    #     stock_code = df_stock_list['code'].values[i]#[:-3]
    #     print(stock_code)
    #     #호가 정보 가져오기
    #     obj_hoga.req_hoga(stock_code)
    #     print('Vol_Offer / Vol_Bid) : ', obj_hoga.vol_ratio_sell_buy())

    #     #호가 정보 저장
    #     obj_hoga.add_10hoga('stgName_None')

    #     #Invest Strategy
    #     obj_hoga.strategy1(obj_hoga.get_stock_name(stock_code), obj_hoga.get_cur_price, 0, 9999999)

#DB 업데이트
    for i in range(len(obj_hoga.stock_df)):
        stock_name = obj_hoga.stock_df.iloc[i]['name']
        print(stock_name)
        obj_hoga.db_update(stock_name)
        obj_hoga.db_show(stock_name)

#1 전략 리스트 조회 예제
    # obj8537 = Cp8537()  
    # data8537 = {}
    # obj8537.Clear()
    # ret, data8537 = obj8537.requestList('내 전략') #내 전략, 예제

    # # 1: 기존 감시 중단 (중요)
    # # 종목검색 실시간 감시 개수 제한이 있어, 불필요한 감시는 중단이 필요
    # obj8537.Clear()

    # # 2 - 종목검색 조회: CpSysDib.CssStgFind
    # stgName = 'M20_over_M60_Buy' #on_fixed_vi' #캔들 연속음봉(10분)'
    # item = data8537[stgName]
    # id = item['ID']
    # name = item['전략명']

    # ret, dataStg = obj8537.requestStgID(id)
    # if ret == False :
    #     print('Search No Result')

    # print('검색전략:', id, '전략명:', name, '검색종목수:', len(dataStg))
    # df_stock_list = pd.DataFrame([], columns=['code', 'name'])
    # for item in dataStg:
    #     item['name'] = item.pop('종목명')
    #     print(item)
    #     df_stock_list = df_stock_list.append(item, ignore_index=True)
    
    # for i in range(len(df_stock_list)):
    #     obj_hoga.req_hoga(df_stock_list.iloc[i]['name'])
    #     obj_hoga.add_10hoga()
    #     print(obj_hoga.stock_df[['name', 'offer[0]', 'cur', 'bid[9]']].iloc[-1].values)
